<!DOCTYPE html>
<html>
  <head>
	  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Template V0.2</title>
	<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="css/core.css" id="css" />
	<link rel="stylesheet" type="text/css" href="css/common.css" />
	<style type="text/css">
body {
  margin-top: 40px;
}
.the-icons li {
  font-size: 14px;
  line-height: 24px;
  height: 24px;
}
/*.switch {
  position: absolute;
  right: 0;
  bottom: 10px;
  color: #666;
}*/
.switch input {
  margin-right: 0.3em;
}
.codesOn .i-name {
  display: none;
}
.codesOn .i-code {
  display: inline;
}
.i-code {
  display: none;
}
</style>
    <link rel="stylesheet" href="css/fontello.css">
	<!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
	<![endif]-->
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <script>
      function toggleCodes(on) {
        var obj = document.getElementById('icons');
        
        if (on) {
          obj.className += ' codesOn';
        } else {
          obj.className = obj.className.replace(' codesOn', '');
        }
      }
      
    </script>
  </head>
  <body>
	<div id="header-fixe">
		<div class="container">
			<header class="row">
				<div class="span8">
					<a href="http://fontello.com/" style="padding-left: 10px">Fontello</a></span>
				</div>
				<div class="span4 text-right">
					<ul>
						<li><a href="./">Démo</a></li>
						<li><a href="icons.php">Icones</a></li>
					</ul>
				</div>
			</header>
		</div>
	</div>
    <div id="icons" class="container">
		<div class="row">
			<div class="span10">
				<label class="switch">
				  <input type="checkbox" onclick="toggleCodes(this.checked)">show codes
				</label>
			</div>
		</div>
      <div class="row">
        <div class="row">
        <div class="span3">
          <ul class="the-icons unstyled">
            <li title="Code: 0x30"><i class="icon-progress-0"></i> <span class="i-name">icon-progress-0</span><span class="i-code">0x30</span></li>
            <li title="Code: 0x31"><i class="icon-progress-1"></i> <span class="i-name">icon-progress-1</span><span class="i-code">0x31</span></li>
            <li title="Code: 0x32"><i class="icon-progress-2"></i> <span class="i-name">icon-progress-2</span><span class="i-code">0x32</span></li>
            <li title="Code: 0x33"><i class="icon-progress-3"></i> <span class="i-name">icon-progress-3</span><span class="i-code">0x33</span></li>
            <li title="Code: 0x34"><i class="icon-progress-4"></i> <span class="i-name">icon-progress-4</span><span class="i-code">0x34</span></li>
            <li title="Code: 0x35"><i class="icon-progress-5"></i> <span class="i-name">icon-progress-5</span><span class="i-code">0x35</span></li>
            <li title="Code: 0x36"><i class="icon-progress-6"></i> <span class="i-name">icon-progress-6</span><span class="i-code">0x36</span></li>
            <li title="Code: 0x37"><i class="icon-progress-7"></i> <span class="i-name">icon-progress-7</span><span class="i-code">0x37</span></li>
            <li title="Code: 0x2190"><i class="icon-left"></i> <span class="i-name">icon-left</span><span class="i-code">0x2190</span></li>
            <li title="Code: 0x2191"><i class="icon-up"></i> <span class="i-name">icon-up</span><span class="i-code">0x2191</span></li>
            <li title="Code: 0x2192"><i class="icon-right"></i> <span class="i-name">icon-right</span><span class="i-code">0x2192</span></li>
            <li title="Code: 0x2193"><i class="icon-down"></i> <span class="i-name">icon-down</span><span class="i-code">0x2193</span></li>
            <li title="Code: 0x21b0"><i class="icon-level-up"></i> <span class="i-name">icon-level-up</span><span class="i-code">0x21b0</span></li>
            <li title="Code: 0x21b3"><i class="icon-level-down"></i> <span class="i-name">icon-level-down</span><span class="i-code">0x21b3</span></li>
            <li title="Code: 0x21c6"><i class="icon-switch"></i> <span class="i-name">icon-switch</span><span class="i-code">0x21c6</span></li>
            <li title="Code: 0x221e"><i class="icon-infinity"></i> <span class="i-name">icon-infinity</span><span class="i-code">0x221e</span></li>
            <li title="Code: 0x229e"><i class="icon-plus-squared"></i> <span class="i-name">icon-plus-squared</span><span class="i-code">0x229e</span></li>
            <li title="Code: 0x229f"><i class="icon-minus-squared"></i> <span class="i-name">icon-minus-squared</span><span class="i-code">0x229f</span></li>
            <li title="Code: 0x2302"><i class="icon-home"></i> <span class="i-name">icon-home</span><span class="i-code">0x2302</span></li>
            <li title="Code: 0x2328"><i class="icon-keyboard"></i> <span class="i-name">icon-keyboard</span><span class="i-code">0x2328</span></li>
            <li title="Code: 0x232b"><i class="icon-erase"></i> <span class="i-name">icon-erase</span><span class="i-code">0x232b</span></li>
            <li title="Code: 0x2389"><i class="icon-pause"></i> <span class="i-name">icon-pause</span><span class="i-code">0x2389</span></li>
            <li title="Code: 0x23e9"><i class="icon-fast-forward"></i> <span class="i-name">icon-fast-forward</span><span class="i-code">0x23e9</span></li>
            <li title="Code: 0x23ea"><i class="icon-fast-backward"></i> <span class="i-name">icon-fast-backward</span><span class="i-code">0x23ea</span></li>
            <li title="Code: 0x23ed"><i class="icon-to-end"></i> <span class="i-name">icon-to-end</span><span class="i-code">0x23ed</span></li>
            <li title="Code: 0x23ee"><i class="icon-to-start"></i> <span class="i-name">icon-to-start</span><span class="i-code">0x23ee</span></li>
            <li title="Code: 0x25a0"><i class="icon-stop"></i> <span class="i-name">icon-stop</span><span class="i-code">0x25a0</span></li>
            <li title="Code: 0x25b4"><i class="icon-up-dir"></i> <span class="i-name">icon-up-dir</span><span class="i-code">0x25b4</span></li>
            <li title="Code: 0x25b6"><i class="icon-play"></i> <span class="i-name">icon-play</span><span class="i-code">0x25b6</span></li>
            <li title="Code: 0x25b8"><i class="icon-right-dir"></i> <span class="i-name">icon-right-dir</span><span class="i-code">0x25b8</span></li>
            <li title="Code: 0x25be"><i class="icon-down-dir"></i> <span class="i-name">icon-down-dir</span><span class="i-code">0x25be</span></li>
            <li title="Code: 0x25c2"><i class="icon-left-dir"></i> <span class="i-name">icon-left-dir</span><span class="i-code">0x25c2</span></li>
            <li title="Code: 0xe817"><i class="icon-cloud-2"></i> <span class="i-name">icon-cloud-2</span><span class="i-code">0xe817</span></li>
            <li title="Code: 0xe824"><i class="icon-cloud"></i> <span class="i-name">icon-cloud</span><span class="i-code">0xe824</span></li>
            <li title="Code: 0xe83c"><i class="icon-star"></i> <span class="i-name">icon-star</span><span class="i-code">0xe83c</span></li>
            <li title="Code: 0xe81d"><i class="icon-star-empty"></i> <span class="i-name">icon-star-empty</span><span class="i-code">0xe81d</span></li>
            <li title="Code: 0x2615"><i class="icon-trash-open"></i> <span class="i-name">icon-trash-open</span><span class="i-code">0x2615</span></li>
            <li title="Code: 0x2661"><i class="icon-heart-empty"></i> <span class="i-name">icon-heart-empty</span><span class="i-code">0x2661</span></li>
            <li title="Code: 0x2665"><i class="icon-heart"></i> <span class="i-name">icon-heart</span><span class="i-code">0x2665</span></li>
            <li title="Code: 0x266a"><i class="icon-note"></i> <span class="i-name">icon-note</span><span class="i-code">0x266a</span></li>
            <li title="Code: 0xe805"><i class="icon-double-note"></i> <span class="i-name">icon-double-note</span><span class="i-code">0xe805</span></li>
            <li title="Code: 0x2691"><i class="icon-flag-2"></i> <span class="i-name">icon-flag-2</span><span class="i-code">0x2691</span></li>
          </ul>
        </div>
        <div class="span3">
          <ul class="the-icons unstyled">
            <li title="Code: 0xe828"><i class="icon-flag"></i> <span class="i-name">icon-flag</span><span class="i-code">0xe828</span></li>
            <li title="Code: 0xe81f"><i class="icon-cog"></i> <span class="i-name">icon-cog</span><span class="i-code">0xe81f</span></li>
            <li title="Code: 0x26a0"><i class="icon-attention"></i> <span class="i-name">icon-attention</span><span class="i-code">0x26a0</span></li>
            <li title="Code: 0x26ab"><i class="icon-record"></i> <span class="i-name">icon-record</span><span class="i-code">0x26ab</span></li>
            <li title="Code: 0x26bf"><i class="icon-key"></i> <span class="i-name">icon-key</span><span class="i-code">0x26bf</span></li>
            <li title="Code: 0x26ef"><i class="icon-cog-alt"></i> <span class="i-name">icon-cog-alt</span><span class="i-code">0x26ef</span></li>
            <li title="Code: 0xe806"><i class="icon-mail"></i> <span class="i-name">icon-mail</span><span class="i-code">0xe806</span></li>
            <li title="Code: 0x270d"><i class="icon-edit"></i> <span class="i-name">icon-edit</span><span class="i-code">0x270d</span></li>
            <li title="Code: 0xe808"><i class="icon-pencil"></i> <span class="i-name">icon-pencil</span><span class="i-code">0xe808</span></li>
            <li title="Code: 0x270e"><i class="icon-pencil-2"></i> <span class="i-name">icon-pencil-2</span><span class="i-code">0x270e</span></li>
            <li title="Code: 0x270f"><i class="icon-pencil-neg"></i> <span class="i-name">icon-pencil-neg</span><span class="i-code">0x270f</span></li>
            <li title="Code: 0x2713"><i class="icon-ok"></i> <span class="i-name">icon-ok</span><span class="i-code">0x2713</span></li>
            <li title="Code: 0x2714"><i class="icon-ok-circle"></i> <span class="i-name">icon-ok-circle</span><span class="i-code">0x2714</span></li>
            <li title="Code: 0xd7"><i class="icon-cancel"></i> <span class="i-name">icon-cancel</span><span class="i-code">0xd7</span></li>
            <li title="Code: 0xe807"><i class="icon-cancel-circled"></i> <span class="i-name">icon-cancel-circled</span><span class="i-code">0xe807</span></li>
            <li title="Code: 0x78"><i class="icon-cancel-circle"></i> <span class="i-name">icon-cancel-circle</span><span class="i-code">0x78</span></li>
            <li title="Code: 0x274e"><i class="icon-cancel-squared"></i> <span class="i-name">icon-cancel-squared</span><span class="i-code">0x274e</span></li>
            <li title="Code: 0x2757"><i class="icon-attention-circle"></i> <span class="i-name">icon-attention-circle</span><span class="i-code">0x2757</span></li>
            <li title="Code: 0x2795"><i class="icon-plus-circled"></i> <span class="i-name">icon-plus-circled</span><span class="i-code">0x2795</span></li>
            <li title="Code: 0x2796"><i class="icon-minus-circled"></i> <span class="i-name">icon-minus-circled</span><span class="i-code">0x2796</span></li>
            <li title="Code: 0xe814"><i class="icon-ccw-2"></i> <span class="i-name">icon-ccw-2</span><span class="i-code">0xe814</span></li>
            <li title="Code: 0xe815"><i class="icon-cw-2"></i> <span class="i-name">icon-cw-2</span><span class="i-code">0xe815</span></li>
            <li title="Code: 0x27f3"><i class="icon-reload"></i> <span class="i-name">icon-reload</span><span class="i-code">0x27f3</span></li>
            <li title="Code: 0xe700"><i class="icon-user-add"></i> <span class="i-name">icon-user-add</span><span class="i-code">0xe700</span></li>
            <li title="Code: 0xe701"><i class="icon-star-half"></i> <span class="i-name">icon-star-half</span><span class="i-code">0xe701</span></li>
            <li title="Code: 0xe702"><i class="icon-ok-circle-2"></i> <span class="i-name">icon-ok-circle-2</span><span class="i-code">0xe702</span></li>
            <li title="Code: 0x2715"><i class="icon-cancel-circle-2"></i> <span class="i-name">icon-cancel-circle-2</span><span class="i-code">0x2715</span></li>
            <li title="Code: 0x3f"><i class="icon-help-circle"></i> <span class="i-name">icon-help-circle</span><span class="i-code">0x3f</span></li>
            <li title="Code: 0x69"><i class="icon-info-circle"></i> <span class="i-name">icon-info-circle</span><span class="i-code">0x69</span></li>
            <li title="Code: 0xe80a"><i class="icon-eye-2"></i> <span class="i-name">icon-eye-2</span><span class="i-code">0xe80a</span></li>
            <li title="Code: 0xe822"><i class="icon-eye"></i> <span class="i-name">icon-eye</span><span class="i-code">0xe822</span></li>
            <li title="Code: 0xe70b"><i class="icon-eye-off"></i> <span class="i-name">icon-eye-off</span><span class="i-code">0xe70b</span></li>
            <li title="Code: 0xe80b"><i class="icon-tag-2"></i> <span class="i-name">icon-tag-2</span><span class="i-code">0xe80b</span></li>
            <li title="Code: 0xe70c"><i class="icon-tag"></i> <span class="i-name">icon-tag</span><span class="i-code">0xe70c</span></li>
            <li title="Code: 0xe70d"><i class="icon-tags"></i> <span class="i-name">icon-tags</span><span class="i-code">0xe70d</span></li>
            <li title="Code: 0xe710"><i class="icon-download-cloud"></i> <span class="i-name">icon-download-cloud</span><span class="i-code">0xe710</span></li>
            <li title="Code: 0xe80e"><i class="icon-upload-cloud-2"></i> <span class="i-name">icon-upload-cloud-2</span><span class="i-code">0xe80e</span></li>
            <li title="Code: 0xe823"><i class="icon-upload-cloud"></i> <span class="i-name">icon-upload-cloud</span><span class="i-code">0xe823</span></li>
            <li title="Code: 0xe80f"><i class="icon-code"></i> <span class="i-name">icon-code</span><span class="i-code">0xe80f</span></li>
            <li title="Code: 0xe715"><i class="icon-export"></i> <span class="i-name">icon-export</span><span class="i-code">0xe715</span></li>
            <li title="Code: 0xe718"><i class="icon-comment"></i> <span class="i-name">icon-comment</span><span class="i-code">0xe718</span></li>
            <li title="Code: 0xe811"><i class="icon-chat-3"></i> <span class="i-name">icon-chat-3</span><span class="i-code">0xe811</span></li>
          </ul>
        </div>
        <div class="span3">
          <ul class="the-icons unstyled">
            <li title="Code: 0xe821"><i class="icon-chat"></i> <span class="i-name">icon-chat</span><span class="i-code">0xe821</span></li>
            <li title="Code: 0xe81e"><i class="icon-chat-2"></i> <span class="i-name">icon-chat-2</span><span class="i-code">0xe81e</span></li>
            <li title="Code: 0xe721"><i class="icon-chat-2-inv"></i> <span class="i-name">icon-chat-2-inv</span><span class="i-code">0xe721</span></li>
            <li title="Code: 0xe722"><i class="icon-vcard"></i> <span class="i-name">icon-vcard</span><span class="i-code">0xe722</span></li>
            <li title="Code: 0xe724"><i class="icon-gmap"></i> <span class="i-name">icon-gmap</span><span class="i-code">0xe724</span></li>
            <li title="Code: 0xe804"><i class="icon-location"></i> <span class="i-name">icon-location</span><span class="i-code">0xe804</span></li>
            <li title="Code: 0xe725"><i class="icon-location-inv"></i> <span class="i-name">icon-location-inv</span><span class="i-code">0xe725</span></li>
            <li title="Code: 0xe729"><i class="icon-trash-close"></i> <span class="i-name">icon-trash-close</span><span class="i-code">0xe729</span></li>
            <li title="Code: 0xe801"><i class="icon-trash"></i> <span class="i-name">icon-trash</span><span class="i-code">0xe801</span></li>
            <li title="Code: 0xe732"><i class="icon-doc-alt"></i> <span class="i-name">icon-doc-alt</span><span class="i-code">0xe732</span></li>
            <li title="Code: 0xe733"><i class="icon-doc-inv-alt"></i> <span class="i-name">icon-doc-inv-alt</span><span class="i-code">0xe733</span></li>
            <li title="Code: 0xe73a"><i class="icon-rss"></i> <span class="i-name">icon-rss</span><span class="i-code">0xe73a</span></li>
            <li title="Code: 0xe73b"><i class="icon-rss-alt"></i> <span class="i-name">icon-rss-alt</span><span class="i-code">0xe73b</span></li>
            <li title="Code: 0xe73d"><i class="icon-cart"></i> <span class="i-name">icon-cart</span><span class="i-code">0xe73d</span></li>
            <li title="Code: 0xe74c"><i class="icon-popup"></i> <span class="i-name">icon-popup</span><span class="i-code">0xe74c</span></li>
            <li title="Code: 0xe800"><i class="icon-popup-2"></i> <span class="i-name">icon-popup-2</span><span class="i-code">0xe800</span></li>
            <li title="Code: 0xe751"><i class="icon-chart-pie"></i> <span class="i-name">icon-chart-pie</span><span class="i-code">0xe751</span></li>
            <li title="Code: 0xe812"><i class="icon-down-circled"></i> <span class="i-name">icon-down-circled</span><span class="i-code">0xe812</span></li>
            <li title="Code: 0xe759"><i class="icon-left-circled"></i> <span class="i-name">icon-left-circled</span><span class="i-code">0xe759</span></li>
            <li title="Code: 0xe75a"><i class="icon-right-circled"></i> <span class="i-name">icon-right-circled</span><span class="i-code">0xe75a</span></li>
            <li title="Code: 0xe813"><i class="icon-up-circled"></i> <span class="i-name">icon-up-circled</span><span class="i-code">0xe813</span></li>
            <li title="Code: 0xe75c"><i class="icon-down-open"></i> <span class="i-name">icon-down-open</span><span class="i-code">0xe75c</span></li>
            <li title="Code: 0x3c"><i class="icon-left-open"></i> <span class="i-name">icon-left-open</span><span class="i-code">0x3c</span></li>
            <li title="Code: 0x3e"><i class="icon-right-open"></i> <span class="i-name">icon-right-open</span><span class="i-code">0x3e</span></li>
            <li title="Code: 0x5e"><i class="icon-up-open"></i> <span class="i-name">icon-up-open</span><span class="i-code">0x5e</span></li>
            <li title="Code: 0xe771"><i class="icon-back-in-time"></i> <span class="i-name">icon-back-in-time</span><span class="i-code">0xe771</span></li>
            <li title="Code: 0xe78e"><i class="icon-off"></i> <span class="i-name">icon-off</span><span class="i-code">0xe78e</span></li>
            <li title="Code: 0xe79a"><i class="icon-brush"></i> <span class="i-name">icon-brush</span><span class="i-code">0xe79a</span></li>
            <li title="Code: 0xe81a"><i class="icon-gauge"></i> <span class="i-name">icon-gauge</span><span class="i-code">0xe81a</span></li>
            <li title="Code: 0xe7a2"><i class="icon-chart-pie-3"></i> <span class="i-name">icon-chart-pie-3</span><span class="i-code">0xe7a2</span></li>
            <li title="Code: 0xe82a"><i class="icon-chart-pie-2"></i> <span class="i-name">icon-chart-pie-2</span><span class="i-code">0xe82a</span></li>
            <li title="Code: 0xe7a3"><i class="icon-chart-pie-3-alt"></i> <span class="i-name">icon-chart-pie-3-alt</span><span class="i-code">0xe7a3</span></li>
            <li title="Code: 0xe7b1"><i class="icon-tablet"></i> <span class="i-name">icon-tablet</span><span class="i-code">0xe7b1</span></li>
            <li title="Code: 0xe840"><i class="icon-firefox"></i> <span class="i-name">icon-firefox</span><span class="i-code">0xe840</span></li>
            <li title="Code: 0xe841"><i class="icon-chrome"></i> <span class="i-name">icon-chrome</span><span class="i-code">0xe841</span></li>
            <li title="Code: 0xe842"><i class="icon-opera"></i> <span class="i-name">icon-opera</span><span class="i-code">0xe842</span></li>
            <li title="Code: 0xe843"><i class="icon-ie"></i> <span class="i-name">icon-ie</span><span class="i-code">0xe843</span></li>
            <li title="Code: 0xf0e6"><i class="icon-chat-empty"></i> <span class="i-name">icon-chat-empty</span><span class="i-code">0xf0e6</span></li>
            <li title="Code: 0xe82b"><i class="icon-facebook"></i> <span class="i-name">icon-facebook</span><span class="i-code">0xe82b</span></li>
            <li title="Code: 0xe82c"><i class="icon-twitter"></i> <span class="i-name">icon-twitter</span><span class="i-code">0xe82c</span></li>
            <li title="Code: 0xe82d"><i class="icon-googleplus"></i> <span class="i-name">icon-googleplus</span><span class="i-code">0xe82d</span></li>
            <li title="Code: 0xe82e"><i class="icon-skype"></i> <span class="i-name">icon-skype</span><span class="i-code">0xe82e</span></li>
          </ul>
        </div>
        <div class="span3">
          <ul class="the-icons unstyled">
            <li title="Code: 0xf313"><i class="icon-youtube"></i> <span class="i-name">icon-youtube</span><span class="i-code">0xf313</span></li>
            <li title="Code: 0x1f304"><i class="icon-picture-2"></i> <span class="i-name">icon-picture-2</span><span class="i-code">0x1f304</span></li>
            <li title="Code: 0xe83e"><i class="icon-picture"></i> <span class="i-name">icon-picture</span><span class="i-code">0xe83e</span></li>
            <li title="Code: 0xe816"><i class="icon-globe"></i> <span class="i-name">icon-globe</span><span class="i-code">0xe816</span></li>
            <li title="Code: 0x1f310"><i class="icon-globe-network"></i> <span class="i-name">icon-globe-network</span><span class="i-code">0x1f310</span></li>
            <li title="Code: 0x1f342"><i class="icon-leaf"></i> <span class="i-name">icon-leaf</span><span class="i-code">0x1f342</span></li>
            <li title="Code: 0x1f3a4"><i class="icon-mic"></i> <span class="i-name">icon-mic</span><span class="i-code">0x1f3a4</span></li>
            <li title="Code: 0x1f3a8"><i class="icon-palette"></i> <span class="i-name">icon-palette</span><span class="i-code">0x1f3a8</span></li>
            <li title="Code: 0x1f3c9"><i class="icon-trophy"></i> <span class="i-name">icon-trophy</span><span class="i-code">0x1f3c9</span></li>
            <li title="Code: 0xe80c"><i class="icon-thumbs-up"></i> <span class="i-name">icon-thumbs-up</span><span class="i-code">0xe80c</span></li>
            <li title="Code: 0xe80d"><i class="icon-thumbs-down"></i> <span class="i-name">icon-thumbs-down</span><span class="i-code">0xe80d</span></li>
            <li title="Code: 0xe825"><i class="icon-user"></i> <span class="i-name">icon-user</span><span class="i-code">0xe825</span></li>
            <li title="Code: 0xe802"><i class="icon-users"></i> <span class="i-name">icon-users</span><span class="i-code">0xe802</span></li>
            <li title="Code: 0x1f4b3"><i class="icon-credit-card"></i> <span class="i-name">icon-credit-card</span><span class="i-code">0x1f4b3</span></li>
            <li title="Code: 0x1f4bb"><i class="icon-monitor"></i> <span class="i-name">icon-monitor</span><span class="i-code">0x1f4bb</span></li>
            <li title="Code: 0x1f4be"><i class="icon-floppy"></i> <span class="i-name">icon-floppy</span><span class="i-code">0x1f4be</span></li>
            <li title="Code: 0x1f4c1"><i class="icon-folder"></i> <span class="i-name">icon-folder</span><span class="i-code">0x1f4c1</span></li>
            <li title="Code: 0x1f4c2"><i class="icon-folder-open"></i> <span class="i-name">icon-folder-open</span><span class="i-code">0x1f4c2</span></li>
            <li title="Code: 0x1f4c5"><i class="icon-calendar"></i> <span class="i-name">icon-calendar</span><span class="i-code">0x1f4c5</span></li>
            <li title="Code: 0x1f4c8"><i class="icon-chart-line"></i> <span class="i-name">icon-chart-line</span><span class="i-code">0x1f4c8</span></li>
            <li title="Code: 0xe829"><i class="icon-chart"></i> <span class="i-name">icon-chart</span><span class="i-code">0xe829</span></li>
            <li title="Code: 0xe818"><i class="icon-chart-bar"></i> <span class="i-name">icon-chart-bar</span><span class="i-code">0xe818</span></li>
            <li title="Code: 0x1f4ca"><i class="icon-chart-bar-2"></i> <span class="i-name">icon-chart-bar-2</span><span class="i-code">0x1f4ca</span></li>
            <li title="Code: 0x1f4ce"><i class="icon-attach"></i> <span class="i-name">icon-attach</span><span class="i-code">0x1f4ce</span></li>
            <li title="Code: 0x1f4e4"><i class="icon-upload"></i> <span class="i-name">icon-upload</span><span class="i-code">0x1f4e4</span></li>
            <li title="Code: 0x1f4e5"><i class="icon-download"></i> <span class="i-name">icon-download</span><span class="i-code">0x1f4e5</span></li>
            <li title="Code: 0xe803"><i class="icon-mobile"></i> <span class="i-name">icon-mobile</span><span class="i-code">0xe803</span></li>
            <li title="Code: 0x1f4f6"><i class="icon-signal"></i> <span class="i-name">icon-signal</span><span class="i-code">0x1f4f6</span></li>
            <li title="Code: 0x1f4f7"><i class="icon-camera"></i> <span class="i-name">icon-camera</span><span class="i-code">0x1f4f7</span></li>
            <li title="Code: 0x1f500"><i class="icon-shuffle"></i> <span class="i-name">icon-shuffle</span><span class="i-code">0x1f500</span></li>
            <li title="Code: 0x1f501"><i class="icon-loop"></i> <span class="i-name">icon-loop</span><span class="i-code">0x1f501</span></li>
            <li title="Code: 0x1f504"><i class="icon-arrows-ccw"></i> <span class="i-name">icon-arrows-ccw</span><span class="i-code">0x1f504</span></li>
            <li title="Code: 0x1f511"><i class="icon-key-inv"></i> <span class="i-name">icon-key-inv</span><span class="i-code">0x1f511</span></li>
            <li title="Code: 0xe826"><i class="icon-lock"></i> <span class="i-name">icon-lock</span><span class="i-code">0xe826</span></li>
            <li title="Code: 0xe827"><i class="icon-unlock"></i> <span class="i-name">icon-unlock</span><span class="i-code">0xe827</span></li>
            <li title="Code: 0xe809"><i class="icon-link"></i> <span class="i-name">icon-link</span><span class="i-code">0xe809</span></li>
            <li title="Code: 0xe820"><i class="icon-wrench-2"></i> <span class="i-name">icon-wrench-2</span><span class="i-code">0xe820</span></li>
            <li title="Code: 0x1f527"><i class="icon-wrench"></i> <span class="i-name">icon-wrench</span><span class="i-code">0x1f527</span></li>
            <li title="Code: 0x1f53e"><i class="icon-chart-area"></i> <span class="i-name">icon-chart-area</span><span class="i-code">0x1f53e</span></li>
            <li title="Code: 0x1f554"><i class="icon-clock"></i> <span class="i-name">icon-clock</span><span class="i-code">0x1f554</span></li>
            <li title="Code: 0x1f6ab"><i class="icon-block"></i> <span class="i-name">icon-block</span><span class="i-code">0x1f6ab</span></li>
          </ul>
        </div>
      </div>
    </div>
  </body>
</html>