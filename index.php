<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Template V0.2</title>
	<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="css/core.css" id="css" />
    <link rel="stylesheet" href="css/fontello.css">
	<!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="css/common.css" />
	<link rel="stylesheet" type="text/css" href="css/prettify.css" />
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body onload="prettyPrint();">
	<div id="ajax-loader"><img src="img/ajax-loader.gif" alt="" /></div>

	<div id="header-fixe">
		<div class="container">
			<header class="row">
				<div class="span8">
					<span style="padding-left: 10px">Template <span class="small">V0.2</span></span>
				</div>
				<div class="span4 text-right">
					<ul>
						<li><a href="./">Démo</a></li>
						<li><a href="icons.php">Icones</a></li>
					</ul>
				</div>
			</header>
		</div>
	</div>
	
	<div id="main" class="container">

		<h1>Navigation</h1>
		<section class="row">
			<div class="span2">
				<h3>Fil d'Arianne</h3>
				<p><a href="" class="toggle" data-toggle="breadcrumb-toggle" data-visible="<strong>Code -</strong>" data-hidden="<strong>Code +</strong>" data-anim="fade"><strong>Code +</strong></a></p>
			</div>
			<div class="span4">
				<ul class="breadcrumb">
					<li>
						<a href="">Home</a> <span class="divider">></span>
					</li>
					<li>
						<a href="">Library</a> <span class="divider">></span>
					</li>
					<li class="active">Data</li>
				</ul>
			</div>
			<div class="span6 hide-begin" id="breadcrumb-toggle">
				<pre class="well prettyprint">
&lt;ul class="breadcrumb">
  &lt;li>
    &lt;a href="">Home&lt;/a> &lt;span class="divider">&gt;&lt;/span>
  &lt;/li>
  &lt;li>
    &lt;a href="">Library&lt;/a> &lt;span class="divider">&gt;&lt;/span>
  &lt;/li>
  &lt;li class="active">Data&lt;/li>
&lt;/ul></pre>
			</div>
		</section>
			<hr />


		<h1>Mise en forme</h1>
		<section class="row">
			<div class="span2">
				<h3>Liens</h3>
			</div>
			<div class="span2">
				<p><a href="">a</a></p>
				<p><a href="" class="external">a.external</a></p>
			</div>
			<div class="span2">
				<p><a href="" class="new-tab">a.new-tab</a></p>
			</div>
		</section>


		<section class="row">
			<div class="span2">
				<h3>Color</h3>
				<p><code>.[color]</code></p>
			</div>
			<div class="span2">
				<p>no class <a href="">a no class</a></p>
				<p class="grey">.grey <a href="" class="grey">a.grey</a></p>
				<p class="pink">.pink <a href="" class="pink">a.pink</a></p>
			</div>
			<div class="span2">
				<p class="black">.black <a href="" class="black">a.black</a></p>
				<p class="white" style="background-color: #EEEEEE">.white <a href="" class="white">a.white</a></p>
			</div>
			<div class="span2">
				<p class="red">.red <a href="" class="red">a.red</a></p>
				<p class="orange">.orange <a href="" class="orange">a.orange</a></p>
			</div>
			<div class="span2">
				<p class="yellow">.yellow <a href="" class="yellow">a.yellow</a></p>
				<p class="green">.green <a href="" class="green">a.green</a></p>
			</div>
			<div class="span2">
				<p class="blue">.blue <a href="" class="blue">a.blue</a></p>
				<p class="violet">.violet <a href="" class="violet">a.violet</a></p>
			</div>
		</section>

		<section class="row">
			<div class="span2">
				<h3>Block well</h3>
				<p><code>.well .[color]</code></p>
			</div>
			<div class="span2">
				<p class="well">.well <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well grey">.well .grey <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well pink">.well .pink <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="well black">.well .black <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well white">.well .white <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="well red">.well .red <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well orange">.well .orange <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="well yellow">.well .yellow <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well green">.well .green <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="well blue">.well .blue <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
				<p class="well violet">.well .violet <span class="ui-icon ui-icon-flag"></span> <a href="">lien</a></p>
			</div>
		</section>
		
		<section class="row">
			<div class="span2">
				<h3>Block alert</h3>
				<p><code>.well .[alert]</code></p>
			</div>
			<div class="span2">
				<p class="well success">.well .success <span class="ui-icon ui-icon-check"></span> <a href="">lien</a></p>
				<p class="success">.success <span class="ui-icon ui-icon-check"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="well error">.well .error <span class="ui-icon ui-icon-closethick"></span> <a href="">lien</a></p>
				<p class="error">.error <span class="ui-icon ui-icon-closethick"></span> <a href="">lien</a></p>
			</div>
			<div class="span2">
				<p class="highlight well">.well .highlight <span class="ui-icon ui-icon-notice"></span> <a href="">lien</a></p>
				<p class="highlight">.highlight <span class="ui-icon ui-icon-notice"></span> <a href="">lien</a></p>
			</div>
		</section>
		
		<section class="row">
			<div class="span2">
				<h3>Badge, Label <span class="bg-btn ui-icon ui-icon-transferthick-e-w"></span></h3>
				<p><code>.label/.badge</code></p>
			</div>
			<div class="span5">
				<p>Labels</p>
				<table class="no-border">
					<tr>
						<td><span class="label">.label</span></td>
						<td><span class="label black">.label .black</span></td>
						<td><span class="label red">.label .red</span></td>
						<td><span class="label yellow">.label .yellow</span></td>
						<td><span class="label blue">.label .blue</span></td>
					</tr>
					<tr>
						<td><span class="label grey">.label .grey</span></td>
						<td><span class="label white">.label .white</span></td>
						<td><span class="label orange">.label .orange</span></td>
						<td><span class="label green">.label .green</span></td>
						<td><span class="label violet">.label .violet</span></td>
					</tr>
					<tr>
						<td colspan="2"><span class="label pink">.label .pink</span></td>
						<td><span class="label success">.label .success</span></td>
						<td><span class="label error">.label .error</span></td>
						<td><span class="label highlight">.label .highlight</span></td>
					</tr>
					<tr>
						<td colspan="5"><a href="" class="label">a.label</a></td>
					</tr>
					<tr>
						<td style="text-align: center;"><a href="" class="toggle" data-toggle="label-hover" data-anim="fade" data-visible="-" data-hidden="+" style="display: block;">+</a></td>
						<td colspan="4"><hr /></td>
					</tr>
				</table>
				<table id="label-hover" class="hide-begin no-border">
					<tr>
						<td><a href="" class="label">a.label</a></td>
						<td><a href="" class="label black">a.label .black</a></td>
						<td><a href="" class="label red">a.label .red</a></td>
						<td><a href="" class="label yellow">a.label .yellow</a></td>
						<td><a href="" class="label blue">a.label .blue</a></td>
					</tr>
					<tr>
						<td><a href="" class="label grey">a.label .grey</a></td>
						<td><a href="" class="label white">a.label .white</a></td>
						<td><a href="" class="label orange">a.label .orange</a></td>
						<td><a href="" class="label green">a.label .green</a></td>
						<td><a href="" class="label violet">a.label .violet</a></td>
					</tr>
					<tr>
						<td colspan="2"><a href="" class="label pink">a.label .pink</a></td>
						<td><a href="" class="label success">a.label .success</a></td>
						<td><a href="" class="label error">a.label .error</a></td>
						<td><a href="" class="label highlight">a.label .highlight</a></td>
					</tr>
					<tr>
						<td colspan="5"><span class="label"><a href="">.label a</a> <a href="" class="error">&times;</a></span></td>
					</tr>
				</table>
			</div>
			<div class="span5">
				<p>Badges</p>
				<table class="no-border">
					<tr>
						<td><span class="badge">.badge</span></td>
						<td><span class="badge black">.badge .black</span></td>
						<td><span class="badge red">.badge .red</span></td>
						<td><span class="badge yellow">.badge .yellow</span></td>
						<td><span class="badge blue">.badge .blue</span></td>
					</tr>
					<tr>
						<td><span class="badge grey">.badge .grey</span></td>
						<td><span class="badge white">.badge .white</span></td>
						<td><span class="badge orange">.badge .orange</span></td>
						<td><span class="badge green">.badge .green</span></td>
						<td><span class="badge violet">.badge .violet</span></td>
					</tr>
					<tr>
						<td colspan="2"><span class="badge pink">.badge .pink</span></td>
						<td><span class="badge success">.badge .success</span></td>
						<td><span class="badge error">.badge .error</span></td>
						<td><span class="badge highlight">.badge .highlight</span></td>
					</tr>
					<tr>
						<td colspan="5"><a href="" class="badge">a.badge</a></td>
					</tr>
					<tr>
						<td style="text-align: center;"><a href="" class="toggle" data-toggle="badge-hover" data-anim="fade" data-visible="-" data-hidden="+" style="display: block;">+</a></td>
						<td colspan="4"><hr /></td>
					</tr>
				</table>
				<table id="badge-hover" class="hide-begin no-border">
					<tr>
						<td><a href="" class="badge">a.badge</a></td>
						<td><a href="" class="badge black">a.badge .black</a></td>
						<td><a href="" class="badge red">a.badge .red</a></td>
						<td><a href="" class="badge yellow">a.badge .yellow</a></td>
						<td><a href="" class="badge blue">a.badge .blue</a></td>
					</tr>
					<tr>
						<td><a href="" class="badge grey">a.badge .grey</a></td>
						<td><a href="" class="badge white">a.badge .white</a></td>
						<td><a href="" class="badge orange">a.badge .orange</a></td>
						<td><a href="" class="badge green">a.badge .green</a></td>
						<td><a href="" class="badge violet">a.badge .violet</a></td>
					</tr>
					<tr>
						<td colspan="2"><a href="" class="badge pink">a.badge .pink</a></td>
						<td><a href="" class="badge success">a.badge .success</a></td>
						<td><a href="" class="badge error">a.badge .error</a></td>
						<td><a href="" class="badge highlight">a.badge .highlight</a></td>
					</tr>
				</table>
			</div>
		</section>
		<hr />
		
		<h1>Boutons / Champs texte</h1>
		<section class="row">
			<p class="span2"><code>*.btn button</code></p>
			<div class="span10">
				<p>
					<a class="btn"><span class="ui-icon ui-icon-trash"></span>a.btn</a> test align
					<button class="btn"><span class="ui-icon ui-icon-trash"></span>button.btn</button>
				</p> 
			</div>
		</section>

		<section class="row">
			<p class="span6"><code>.[input-append/input-prepend] [.add-on]</code></p>
			<div class="span3">
				<p class="input-prepend input-append">
					<input type="button" class="add-on" value="input" /><input type="text" size="16"><span class="add-on">[button]</span>
				</p>
			</div>
			<div class="span3">
				<p class="input-prepend input-append">
					<input type="submit" class="add-on" value="input" /><input type="text" size="16"><span class="add-on">[submit]</span>
				</p>
			</div>
		</section>
		<section class="row">
			<div class="span3">
				<p class="input-prepend input-append">
					<button class="add-on">button</button><input type="text" size="16"><a href="" class="add-on btn">a.btn</a>
				</p>
			</div>
			<div class="span3">
				<p class="input-prepend input-append">
					<span class="add-on btn">.btn</span><input type="text" size="4"><span class="add-on btn">.btn</span><span class="add-on btn">.btn</span>
				</p>
			</div>
			<div class="span3">
				<p class="input-prepend input-append">
					<span class="add-on clickable white">.clickable.white</span><input type="text" size="6"><span class="add-on clickable">.clickable</span>
				</p>
			</div>
			<div class="span3">
				<p class="input-prepend">
					<span class="add-on clickable blue">.clickable.blue</span><input type="text" size="14" class="blue" placeholder=".blue">
				</p>
			</div>
		</section>
		<section class="row">
			<div class="span3">
				<p class="input-append">
					<input type="text" size="24" placeholder="select"><span class="add-on"><select><option>&euro;</option><option>&dollar;</option></select></span>
				</p>
			</div>
			<div class="span3">
				<p class="input-append">
					<input type="text" size="24" placeholder="button span.ui-icon"><button class="add-on dropdown" data-dropdown="dropdown-input-append"><span class="ui-icon ui-icon-triangle-1-s"></span></button></button>
					<div id="dropdown-input-append" class="well white">Test de <br />Dropdown</div>
				</p>
			</div>
		</section>

		<section class="row">
			<p class="span2"><code>input.span[1-12]</code></p>
			<input type="text" class="span1" placeholder=".span1" />
			<input type="text" class="span2" placeholder=".span2" />
			<input type="text" class="span3" placeholder=".span3" />
			<input type="text" class="span4" placeholder=".span4" />
		</section>

		<section class="row">
			<p class="span2"><code>input.[color/alert]</code></p>
			<p class="span2">
				<input type="text" class="" placeholder="normal" /><br />
				<input type="text" class="grey" placeholder=".grey" /><br />
				<input type="text" class="pink" placeholder=".pink" />
			</p>
			<p class="span2">
				<input type="text" class="black" placeholder=".black" /><br />
				<input type="text" class="white" placeholder=".white" />
			</p>
			<p class="span2">
				<input type="text" class="red" placeholder=".red" /><br />
				<input type="text" class="orange" placeholder=".orange" /><br />
				<input type="text" class="success" placeholder=".success" />
			</p>
			<p class="span2">
				<input type="text" class="yellow" placeholder=".yellow" /><br />
				<input type="text" class="green" placeholder=".green" /><br />
				<input type="text" class="error" placeholder=".error" />
			</p>
			<p class="span2">
				<input type="text" class="blue" placeholder=".blue" /><br />
				<input type="text" class="violet" placeholder=".violet" /><br />
				<input type="text" class="highlight" placeholder=".highlight" />
			</p>
		</section>
		<hr />
		
		<h1>Clickable</h1>
		<section class="row">
			<div class="span2">
				<a href="" class="clickable">a.clickable <span class="ui-icon ui-icon-triangle-1-s"></span></a>
				<div class="clickable">div.clickable <span class="ui-icon ui-icon-triangle-1-s"></span></div>
			</div>
			<div class="span2">
				<a href="" class="clickable grey">a.clickable .grey <span class="ui-icon ui-icon-triangle-1-s"></span></a><br />
				<a href="" class="clickable pink">a.clickable .pink <span class="ui-icon ui-icon-triangle-1-s"></span></a>
			</div>
			<div class="span2">
				<a href="" class="clickable black">a.clickable .black <span class="ui-icon ui-icon-triangle-1-s"></span></a><br />
				<a href="" class="clickable white">a.clickable .white <span class="ui-icon ui-icon-triangle-1-s"></span></a>
			</div>
			<div class="span2">
				<a href="" class="clickable red">a.clickable .red <span class="ui-icon ui-icon-triangle-1-s"></span></a><br />
				<a href="" class="clickable orange">a.clickable .orange <span class="ui-icon ui-icon-triangle-1-s"></span></a>
			</div>
			<div class="span2">
				<a href="" class="clickable yellow">a.clickable .yellow <span class="ui-icon ui-icon-triangle-1-s"></span></a><br />
				<a href="" class="clickable green">a.clickable .green <span class="ui-icon ui-icon-triangle-1-s"></span></a>
			</div>
			<div class="span2">
				<a href="" class="clickable blue">a.clickable .blue <span class="ui-icon ui-icon-triangle-1-s"></span></a><br />
				<a href="" class="clickable violet">a.clickable .violet <span class="ui-icon ui-icon-triangle-1-s"></span></a>
			</div>
		</section>
		<hr />
		
		<h1>Table</h1>
		<section class="row">
			<div class="span2">
				<code>table</code>
				<table>
					<thead>
						<tr>
							<th>thead</th>
							<th>thead</th>
							<th>thead</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>tfoot</td>
							<td>tfoot</td>
							<td>tfoot</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="span2">
				<code>table.bordered</code>
				<table class="bordered">
					<thead>
						<tr>
							<th>thead</th>
							<th>thead</th>
							<th>thead</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="2">td</td>
							<td>td</td>
							<td>td</td>
						</tr>
						<tr>
							<td colspan="2">td</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>tfoot</td>
							<td>tfoot</td>
							<td>tfoot</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="span2">
				<code>table.stripped</code>
				<table class="stripped">
					<thead>
						<tr>
							<th>thead</th>
							<th>thead</th>
							<th>thead</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>tfoot</td>
							<td>tfoot</td>
							<td>tfoot</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="span2">
				<code>table.separate</code>
				<table class="separate">
					<thead>
						<tr>
							<th>thead</th>
							<th>thead</th>
							<th>thead</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>tfoot</td>
							<td>tfoot</td>
							<td>tfoot</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="span2">
				<code>table.no-border</code>
				<table class="no-border">
					<thead>
						<tr>
							<th>thead</th>
							<th>thead</th>
							<th>thead</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
						<tr>
							<td>td</td>
							<td>td</td>
							<td>td</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>tfoot</td>
							<td>tfoot</td>
							<td>tfoot</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</section>
		<hr />
		
		<h1>Classes annexes</h1>
		<section class="row">
			<div class="span2">
				<h3>Shadow</h3>
				<p><code>.shadow-[type]</code></p>
			</div>
			<div class="span2">
				<p class="well shadow-outset">.shadow-outset</p>
				<p class="well shadow-inset">.shadow-inset</p>
			</div>
			<div class="span2">
				<p class="well shadow-hover">.shadow-hover</p>
				<p class="well shadow-hover red">.shadow-hover .red</p>
			</div>
			<div class="span2">
				<p class="well shadow-outset blue">.shadow-outset .blue</p>
				<p class="well shadow-inset success">.shadow-inset .success</p>
			</div>
		</section>
		
		<section class="row">
			<div class="span2">
				<h3>Fade</h3>
				<p><code>.fade-[in/out]-hover</code></p>
			</div>
			<div class="span2">
				<p class="well fade">.fade</p>
			</div>
			<div class="span2">
				<p class="well fade fade-in-hover">.fade.fade-in-hover</p>
			</div>
			<div class="span2">
				<p class="well fade-out-hover">.fade-out-hover</p>
			</div>
		</section>
		
		<section class="row">
			<div class="span2">
				<h3>Rotate</h3>
				<p><code>.rotate-[l/r]<br />-[45/180/360]<br />[-hover]</code></p>
			</div>
			<div class="span2">
				<p class="well error"><span class="ui-icon ui-icon-heart rotate-l-45"></span> .rotate-l-45</p>
			</div>
			<div class="span2">
				<p class="well violet"><span class="ui-icon ui-icon-wrench rotate-r-45-hover"></span> .rotate-r-45-hover</p>
			</div>
			<div class="span2">
				<p class="well blue"><span class="ui-icon ui-icon-arrowrefresh-1-e rotate-r-360-hover"></span> .rotate-r-360-hover</p>
			</div>
		</section>

		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<div class="span2">
				<h3>Font loader</h3>
				<code>.icon-progress</code>
			</div>
			<div class="span2">
				<p class="well"><span class="icon-progress"></span> .icon-progress</span></p>
				<p class="well red big"><span class="icon-progress"></span> .icon-progress</span></p>
				<p class="well highlight small fade"><span class="icon-progress"></span> .icon-progress</span></p>
			</div>
			<div class="span3">
				<p class="well"><span class="icon-progress fade-out-hover"></span> .icon-progress .fade-out-hover</span></p>
				<p class="well"><span class="icon-progress icon-progress-4"></span> .icon-progress .icon-progress-4</span></p>
				<p class="well"><span class="icon-progress big orange"></span> .icon-progress .big .orange</span></p>
			</div>
			
		</section>
		
		<section class="row">
			<div class="span2">
				<h3>Styles</h3>
			</div>
			<div class="span2">
				<p class="discret">.discret</p>
			</div>
			<div class="span2">
				<p>normal <span  class="small">.small</span></p>
				<p>normal <span class="big">.big</span></p>
			</div>
			<div class="span2">
				<p class="bold">.bold</p>
				<p class="italic">.italic</p>
			</div>
			<div class="span2">
				<p class="underline">.underline</p>
				<p class="overline">.overline</p>
			</div>
			<div class="span2">
				<p class="line-through">.line-through</p>
			</div>
		</section>
		
		
		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<div class="span2">
				<h3>Classes d'aide</h3>
			</div>
			<div class="span4">
				<dl>
					<dt>.hide</dt>
					<dd>Cache l'élément</dd>
					
					<dt>.show</dt>
					<dd>Affiche l'élément (display: block)</dd>
					
					<dt>.hide-begin</dt>
					<dd>Cache l'élément uniquement au chargement de la page <small><em>(permet de différencier de .hide si uniquement besoin de cacher au départ)</em></small></dd>
				</dl>
			</div>
			<div class="span3">
				<dl>
					<dt>.text-[left,center,right,justify]</dt>
					<dd>CSS-> text-align</dd>
					
					<dt>.float-[left,right]</dt>
					<dd>CSS-> float</dd>
					
					<dt>.clear / .clearfix</dt>
					<dd>CSS-> clear: both</dd>
					
					<dt>.no-padding/.no-margin</dt>
					<dd>Supprime le padding/margin</dd>
				</dl>
			</div>
			<div class="span3">
				<dl>
					<dt>.pointer</dt>
					<dd>CSS-> cursor: pointer</dd>
				</dl>
			</div>
		</section>
		<hr />
		
		<h1>Javascript</h1>
		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<div class="span2">
				<h3>Dropdown</h3>
			</div>
			<div class="span4">
				<code>class="dropdown" data-dropdown="dropdown_div"</code><br /><input type="button" class="dropdown" data-dropdown="dropdown_div" value="Dropdown" />
				<div id="dropdown_div" class="well" style="display: inline-block;">Test de <br />Dropdown</div>
				<br />
				<code>class="dropdown" data-dropdown="dropdown_div" data-dropdown-my="right bottom" data-dropdown-at="right top" data-dropdown-direction="down"</code><br /><input type="button" class="dropdown" data-dropdown="dropdown_div3" data-dropdown-my="right bottom" data-dropdown-at="right top" value="Dropdown" data-dropdown-direction="down" />
				<div id="dropdown_div3" class="well" style="display: inline-block;">Test de <br />Dropdown</div>
			</div>
			<div class="span6">
				<code>class="clickable dropdown blue" data-dropdown="dropdown_div2"</code><br />
				<p class="clickable dropdown blue" data-dropdown="dropdown_div2" style="display: inline-block">div.clickable <span class="ui-icon ui-icon-triangle-1-s"></span></p>
				<div id="dropdown_div2" class="well blue" style="display: inline-block;">Test de <br />Dropdown</div>
				<br />
				<code>class="clickable dropdown blue" data-dropdown="dropdown_div4" data-dropdown-duration="slow" data-dropdown-effect="fade" data-dropdown-easing="easeOutQuint"</code><br />
				<button class="dropdown" data-dropdown="dropdown_div4" data-dropdown-duration="slow" data-dropdown-effect="fade" data-dropdown-easing="easeOutQuint" style="display: inline-block">Dropdown</button>
				<div id="dropdown_div4" class="well" style="display: inline-block;">Test de <br />Dropdown</div>
			</div>
		</section>
		
		
		<section class="row">
			<div class="span2">
				<h3>Confirm</h3>
			</div>
			<div class="span2">
				<code>$('.confirm').confirm();</code>
				<a href="" class="confirm">confirm sur un lien</a>
			</div>
			<div class="span2">
				<code>$('.confirm').confirm();</code>
				<form id="form-confirm" action=""><input type="submit" class="confirm" value="confirm sur un submit" /></form>
			</div>
			<div class="span6">
				<code>$('.confirm').confirm();</code><br />
				<code>&lt;a href="" class="confirm" data-confirm-title="Mon titre HTML" <br />data-confirm-message="Mon Message HTML !!" <br />data-confirm-button-ok="Je valide !" <br />data-confirm-button-cancel="J'annule !"<br />data-confirm-button-ok-icon-primary="ui-icon-check"<br />data-confirm-button-cancel-icon-primary="ui-icon-closethick"&gt;<br />data-confirm-button-cancel-icon-secondary="ui-icon-notice"&gt;</code><br />
				<a href="" class="confirm" data-confirm-title="Mon titre HTML" data-confirm-message="Mon Message HTML !!" data-confirm-button-ok="Je valide !" data-confirm-button-cancel="J'annule" data-confirm-button-ok-icon-primary="ui-icon-check" data-confirm-button-cancel-icon-primary="ui-icon-closethick" data-confirm-button-cancel-icon-secondary="ui-icon-notice">confirm sur un lien avec params data-html</a>
			</div>
		</section>
		<section class="row">
			<div class="span10 offset2">
			<hr />
				<a href="" class="confirm-params-js">confirm sur un lien avec params js</a> - <a href="" class="toggle" data-toggle="confirm-params-js-code" data-anim="fade" data-visible="Code -" data-hidden="Code +" >Code +</a>
				<span id="confirm-param-js-output"></span>
				<pre class="well prettyprint" id="confirm-params-js-code" style="display: none;">
$('.confirm-params-js').confirm({
  title: 'Mon titre',
  message: 'Mon Message&lt;br />Sur deux lignes',
  button_ok: 'OUI !',
  button_cancel: 'NON !',
  button_cancel_icon_primary: 'ui-icon-alert',
  ok: function() {
    $('#confirm-param-js-output')
      .attr('class', 'well success')
      .html('Ok !');
  },
  cancel: function () {
    $('#confirm-param-js-output')
      .attr('class', 'well error')
      .html('Canceled !');
  }
});</pre>
			</div>
		</section>
		
		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<hr />
			<div class="span2">
				<h3>Carousel</h3>
				<p><a href="" class="toggle" data-toggle="carousel-toggle" data-visible="<strong>Exemples -</strong>" data-hidden="<strong>Exemples +</strong>" data-anim="drop"><strong>Exemples +</strong></a></p>
			</div>
			<div class="span3">
				<div id="carousel">
					<div></div><div></div><div></div><div></div><div></div><div></div>
				</div>
				<div id="carousel-result">slide numéro: 0</div>
			</div>
			<div class="span7">
				<div id="carousel-1-tabs">
					<ul>
						<li><a href="#js-carousel-1">Javascript</a></li>
						<li><a href="#html-carousel-1">HTML</a></li>
						<li><a href="#css-carousel-1">CSS</a></li>
					</ul>
<pre id="js-carousel-1" class="prettyprint">
$('#carousel').carousel().bind('carousel', function(event, direction) { 
    $('#carousel-result').html(
		'slide numéro: '+$(this).attr('data-current-num')+' - '+direction
	);
});</pre>
<pre id="html-carousel-1" class="prettyprint">
&lt;div id="carousel">
	&lt;div>&lt;/div>[... *4]&lt;div>&lt;/div>
&lt;/div>
&lt;div id="carousel-result">slide numéro: 0&lt;/div></pre>
<pre id="css-carousel-1" class="prettyprint">
#carousel div {
	display: inline-block;
	width: 100px;
	height: 160px;
}</pre>
				</div>
			</div>
			
		</section>
		<div id="carousel-toggle" style="display: none;">
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span8">
					<div id="carousel-2-tabs">
						<ul>
							<li><a href="#js-carousel-2">Javascript</a></li>
							<li><a href="#html-carousel-2">HTML</a></li>
							<li><a href="#css-carousel-2">CSS</a></li>
						</ul>
<pre id="js-carousel-2" class="prettyprint">
$('#carousel2').carousel({
	vertical: true,
	loop: true
});</pre>
<pre id="html-carousel-2" class="prettyprint">
&lt;div id="carousel2">
	&lt;div>1&lt;/div>[... *4]&lt;div>6&lt;/div>
&lt;/div></pre>
<pre id="css-carousel-2" class="prettyprint">
#carousel2 div {
	display: block;
}</pre>
					</div>
				</div>
				<div class="span4">
					<div id="carousel-2">
						<div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div>
					</div>
				</div>
			</section>
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span2">
					<div id="carousel-3">
						<div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div>
					</div>
				</div>
				<div class="span2">
					<div id="carousel-3-result" style="height: 290px; overflow: auto;"></div>
				</div>
				<div class="span8">
					<div id="carousel-3-tabs">
						<ul>
							<li><a href="#js-carousel-3">Javascript</a></li>
							<li><a href="#html-carousel-3">HTML</a></li>
							<li><a href="#css-carousel-3">CSS</a></li>
						</ul>
<pre id="js-carousel-3" class="prettyprint">
$('#carousel3').carousel({
  can_disable: false,
  duration: 'slow',
  easing: 'easeInOutQuart', /* voir http://jqueryui.com/demos/effect/easing.html */
  onbeforeslide: function () {
	$('#carousel3-result').append('Before Slide ! n° '+$('#carousel3').attr('data-current-num'));
  },
  onslide: function () {
	$('#carousel3-result').append('Sliding ! n° '+$('#carousel3').attr('data-current-num'));
  },
  onafterslide: function () {
	$('#carousel3-result').append('After Slide ! n° '+$('#carousel3').attr('data-current-num'));
  }
});</pre>
<pre id="html-carousel-3" class="prettyprint">
&lt;div id="carousel3">
	&lt;div>1&lt;/div>[... *4]&lt;div>6&lt;/div>
&lt;/div>
&lt;div id="carousel3-result">&lt;/div></pre>
					</div>
				</div>
			</section>
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span7">
					<div id="carousel-4-tabs">
						<ul>
							<li><a href="#js-carousel-4">Javascript</a></li>
							<li><a href="#html-carousel-4">HTML</a></li>
							<li><a href="#css-carousel-4">CSS</a></li>
						</ul>
<pre id="js-carousel-4" class="prettyprint">
$('#carousel4').carousel({
	nb_shown: 2, nb_move: 2
});
$('#carousel-4-pages div').click( function() { 
	$('#carousel-4').trigger('slideTo', $(this).html());
});</pre>
<pre id="html-carousel-4" class="prettyprint">
&lt;div id="carousel4">
	&lt;div>1&lt;/div>[... *4]&lt;div>6&lt;/div>
&lt;/div>
&lt;div id="carousel-4-pages">
	&lt;div class="clickable">1&lt;/div>[... *4]&lt;div class="clickable">6&lt;/div>
&lt;/div></pre>
<pre id="css-carousel-4" class="prettyprint">
#carousel-4-pages div {
	width: 16px;
	height: 16px;
	text-align: center;
	margin-top: 4px;
}</pre>
					</div>
				</div>
				<div class="span3 offset1">
					<div id="carousel-4">
						<div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div>
					</div>
				</div>
				<div class="span1">
					<div id="carousel-4-pages">
						<div class="clickable">1</div><div class="clickable">2</div><div class="clickable">3</div><div class="clickable">4</div><div class="clickable">5</div><div class="clickable">6</div>
					</div>
				</div>
			</section>
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span4 offset2">
					<div id="carousel-5">
						<div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div>
					</div>
				</div>
				<div class="span6">
					<pre class="prettyprint">
$('#carousel5').carousel({
	autoSlide: true,
	autoSlide_duration: 2000
});
<hr />&lt;div id="carousel5">
	&lt;div>1&lt;/div>[... *4]&lt;div>6&lt;/div>
&lt;/div></pre>
				</div>
			</section>
		</div>
		
		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<hr />
			<div class="span2">
				<h3>Pagination</h3>
				<p><a href="" class="toggle" data-toggle="pagination-toggle" data-visible="<strong>Exemples -</strong>" data-hidden="<strong>Exemples +</strong>" data-anim="drop"><strong>Exemples +</strong></a></p>
			</div>
			<div class="span3">
				<div class="pagination-conteneur" style="margin-top: 50px;">
					<ul id="pagination-contenu">
						<li>- li 1</li><li>- li 2</li><li>- li 3</li><li>- li 4</li><li>- li 5</li><li>- li 6</li>
						<li>- li 7</li><li>- li 8</li><li>- li 9</li><li>- li 10</li><li>- li 11</li><li>- li 12</li>
						<li>- li 13</li><li>- li 14</li><li>- li 15</li><li>- li 16</li><li>- li 17</li><li>- li 18</li>
						<li>- li 19</li><li>- li 20</li><li>- li 21</li><li>- li 22</li><li>- li 23</li><li>- li 24</li>
						<li>- li 25</li><li>- li 26</li><li>- li 27</li><li>- li 28</li><li>- li 29</li><li>- li 30</li>
						<li>- li 31</li><li>- li 32</li><li>- li 33</li><li>- li 34</li><li>- li 35</li><li>- li 36</li>
					</ul>
					<div id="pagination"></div>
				</div>
			</div>
			<div class="span7">
				<div id="pagination-tabs">
					<ul>
						<li><a href="#js-pagination">Javascript</a></li>
						<li><a href="#html-pagination">HTML</a></li>
						<li><a href="#css-pagination">CSS</a></li>
					</ul>
<pre id="js-pagination" class="prettyprint">
$('#pagination').pagination({
	target: 'pagination-contenu',
	nb_by_pages: 3
});</pre>
<pre id="html-pagination" class="prettyprint">
&lt;ul id="pagination-contenu">
	&lt;li>- li 1&lt;/li>
	[... *34]
	&lt;li>- li 36&lt;/li>
&lt;/ul>
&lt;div id="pagination">&lt;/div></pre>

				</div>
			</div>
			
		</section>
		<div id="pagination-toggle">
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span2">
					<div class="pagination-conteneur" style="margin-top: 50px;">
						<ul id="pagination-2-contenu">
							<li>- li 1</li><li>- li 2</li><li>- li 3</li><li>- li 4</li><li>- li 5</li><li>- li 6</li>
							<li>- li 7</li><li>- li 8</li><li>- li 9</li><li>- li 10</li><li>- li 11</li><li>- li 12</li>
							<li>- li 13</li><li>- li 14</li><li>- li 15</li><li>- li 16</li><li>- li 17</li><li>- li 18</li>
							<li>- li 19</li><li>- li 20</li><li>- li 21</li><li>- li 22</li><li>- li 23</li><li>- li 24</li>
							<li>- li 25</li><li>- li 26</li><li>- li 27</li><li>- li 28</li><li>- li 29</li><li>- li 30</li>
							<li>- li 31</li><li>- li 32</li><li>- li 33</li><li>- li 34</li><li>- li 35</li><li>- li 36</li>
						</ul>
						<div id="pagination-2"></div>
					</div>
				</div>
				<div class="span4">
					<div id="pagination-2-tabs">
						<ul>
							<li><a href="#js-pagination-2">Javascript</a></li>
							<li><a href="#html-pagination-2">HTML</a></li>
							<li><a href="#css-pagination-2">CSS</a></li>
						</ul>
<pre id="js-pagination-2" class="prettyprint">
$('#pagination-2').pagination({
	target: 'pagination-2-contenu',
	classes: 'clickable black',
	effect: 'drop',
	nb_by_pages: 5,
	nb_shown: 3
});</pre>
<pre id="html-pagination-2" class="prettyprint">
&lt;ul id="pagination-2-contenu">
	&lt;li>- li 1&lt;/li>
	[... *34]
	&lt;li>- li 36&lt;/li>
&lt;/ul>
&lt;div id="pagination-2">&lt;/div></pre>

					</div>
				</div>
				
				
				<div class="span2">
					<div class="pagination-conteneur" style="margin-top: 10px;">
						<ul id="pagination-3-contenu">
							<li>- li 1</li><li>- li 2</li><li>- li 3</li><li>- li 4</li><li>- li 5</li><li>- li 6</li>
							<li>- li 7</li><li>- li 8</li><li>- li 9</li><li>- li 10</li><li>- li 11</li><li>- li 12</li>
							<li>- li 13</li><li>- li 14</li><li>- li 15</li><li>- li 16</li><li>- li 17</li><li>- li 18</li>
							<li>- li 19</li><li>- li 20</li><li>- li 21</li><li>- li 22</li><li>- li 23</li><li>- li 24</li>
							<li>- li 25</li><li>- li 26</li><li>- li 27</li><li>- li 28</li><li>- li 29</li><li>- li 30</li>
							<li>- li 31</li><li>- li 32</li><li>- li 33</li><li>- li 34</li><li>- li 35</li><li>- li 36</li>
						</ul>
						<div id="pagination-3"></div>
					</div>
					<div class="pagination-conteneur" style="margin-top: 25px;">
						<ul id="pagination-4-contenu">
							<li>- li 1</li><li>- li 2</li><li>- li 3</li><li>- li 4</li><li>- li 5</li><li>- li 6</li>
							<li>- li 7</li><li>- li 8</li><li>- li 9</li><li>- li 10</li><li>- li 11</li><li>- li 12</li>
							<li>- li 13</li><li>- li 14</li><li>- li 15</li><li>- li 16</li><li>- li 17</li><li>- li 18</li>
							<li>- li 19</li><li>- li 20</li><li>- li 21</li><li>- li 22</li><li>- li 23</li><li>- li 24</li>
							<li>- li 25</li><li>- li 26</li><li>- li 27</li><li>- li 28</li><li>- li 29</li><li>- li 30</li>
							<li>- li 31</li><li>- li 32</li><li>- li 33</li><li>- li 34</li><li>- li 35</li><li>- li 36</li>
						</ul>
						<div id="pagination-4"></div>
					</div>
				</div>
				<div class="span4">
					<div id="pagination-3-tabs">
						<ul>
							<li><a href="#js-pagination-3">Javascript</a></li>
							<li><a href="#html-pagination-3">HTML</a></li>
							<li><a href="#css-pagination-3">CSS</a></li>
						</ul>
<pre id="js-pagination-3" class="prettyprint">
$('#pagination-3').pagination({
	target: 'pagination-3-contenu',
	classes: 'well white',
});<hr />$('#pagination-4').pagination({
	target: 'pagination-4-contenu',
	classes: 'my-pagination-custom',
});</pre>
<pre id="html-pagination-3" class="prettyprint">
&lt;ul id="pagination-3-contenu">
[...]
&lt;div id="pagination-3">&lt;/div><hr />&lt;ul id="pagination-4-contenu">
[...]
&lt;div id="pagination-4">&lt;/div></pre>
<pre id="css-pagination-3" class="prettyprint">
.my-pagination-custom {
  width: 22px; height: 30px;
  border: 2px solid white;
  font-size: 10px;
  font-family: "Comic Sans MS";
  background: linear-gradient(<abbr title="to right, #ebe9f9 0%,#d8d0ef 50%,#cec7ec 51%,#c1bfea 100%">[...]</abbr>);
}
.my-pagination-custom.active {
  color: red; background-clip: content-box;
  border: 2px solid transparent !important;
}</pre>

					</div>
				</div>

			</section>
			<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
				<div class="span2">
					<div class="pagination-conteneur" style="margin-top: 50px;">
						<div id="pagination-ajax-contenu">
						</div>
						<div id="pagination-ajax"></div>
					</div>
				</div>
				<div class="span4">
					<div id="pagination-ajax-tabs">
						<ul>
							<li><a href="#js-pagination-ajax">Javascript</a></li>
							<li><a href="#html-pagination-ajax">HTML</a></li>
							<li><a href="#css-pagination-ajax">CSS</a></li>
							<li><a href="#php-pagination-ajax">PHP</a></li>
						</ul>
<pre id="js-pagination-ajax" class="prettyprint">
$('#pagination-ajax').pagination({
	target: 'pagination-ajax-contenu',
	classes: 'clickable white',
	effect: 'explode',
	speed_out: 'slow', speed_in: 'slow',
	nb_by_pages: 2, nb_shown: 3,
	ajax: {
		'url' : 'pagination.ajax.php',
		'nb_total': 25,
		'datas' : {'ma_data': 15}
	}
});</pre>
<pre id="html-pagination-ajax" class="prettyprint">
&lt;div id="pagination-ajax-contenu">
&lt;/div>
&lt;div id="pagination-ajax">&lt;/div>
</pre>
<pre id="php-pagination-ajax" class="prettyprint">
&lt;?php
mysql_query("SELECT [...] 
  LIMIT ".
  $_POST['current_page']*$_POST['$nb_by_pages']
  .", ".$_POST['$nb_by_pages']);
echo $datas;
</pre>

					</div>
				</div>
				
				
				<div class="span2">
					<div class="pagination-conteneur">
						<div id="pagination-ajax-2-contenu">
						</div>
						<div id="pagination-ajax-2"></div>
					</div>
				</div>
				<div class="span4">
					<div id="pagination-ajax-2-tabs">
						<ul>
							<li><a href="#js-pagination-ajax-2">Javascript</a></li>
							<li><a href="#html-pagination-ajax-2">HTML</a></li>
							<li><a href="#css-pagination-ajax-2">CSS</a></li>
							<li><a href="#php-pagination-ajax-2">PHP</a></li>
						</ul>
<pre id="js-pagination-ajax-2" class="prettyprint">
$('#pagination-ajax-2').pagination({
	target: 'pagination-ajax-2-contenu',
	classes: 'clickable',
	method: 'post',
	nb_by_pages: 5, nb_shown: 2,
	ajax: {
		'url' : 'pagination.ajax.php',
		'nb_total': 25,
		'json': true
	}
});</pre>
<pre id="html-pagination-ajax-2" class="prettyprint">
&lt;div id="pagination-ajax-2-contenu">
&lt;/div>
&lt;div id="pagination-ajax-2">&lt;/div>
</pre>
<pre id="php-pagination-ajax-2" class="prettyprint">
&lt;?php
mysql_query("SELECT [...] 
  LIMIT ".
  $_POST['current_page']*$_POST['$nb_by_pages']
  .", ".$_POST['$nb_by_pages']);
echo json_encode($datas);
</pre>

					</div>
				</div>

			</section>
		</div>
		
		<hr />
		
		<section class="row" style="margin-top: 4px; margin-bottom: 10px;">
			<hr />
			<div class="span2">
				<h3>Scroll Infini</h3>
			</div>
			<div class="span3">
				<ul id="infinite-scroll">
					<li class="infinite-loader">Loading</li>
					<li class="infinite-error">Error</li>
				</ul>
			</div>
			<div class="span7">
				<div id="infinite-scroll-tabs">
					<ul>
						<li><a href="#js-infinite-scroll">Javascript</a></li>
						<li><a href="#html-infinite-scroll">HTML</a></li>
						<li><a href="#css-infinite-scroll">CSS</a></li>
						<li><a href="#php-infinite-scroll">PHP</a></li>
					</ul>
<pre id="js-infinite-scroll" class="prettyprint">
$('#infinite-scroll').infiniteScroll({
	nb_to_show: 6,  'speed_out': 'slow',  'speed_in' : 'slow',
	ajax: {
		'url' : 'infiniteScroll.ajax.php',  'json': true,
		'datas' : {'ma_data': 15}
	}
});</pre>
<pre id="html-infinite-scroll" class="prettyprint">
&lt;ul> id="infinite-scroll">
	&lt;li class="infinite-loader">Loading&lt;/li>
	&lt;li class="infinite-error">Error&lt;/li>
&lt;/ul></pre>
<pre id="css-infinite-scroll" class="prettyprint">
#infinite-scroll {
	max-height: 100px;
	overflow: auto;
}</pre>
<pre id="php-infinite-scroll" class="prettyprint">
&lt;?php
	mysql_query("SELECT [...] LIMIT ". $_POST['current'] .", "+$_POST['nb_to_show']);
	
	echo json_encode($datas); ?>
</pre>

				</div>
			</div>
			
		</section>

		<hr />
		<h1>Formulaire</h1>
		<section class="row">
			<h3 class="span2">
				Formulaire
			</h3>
			<div class="span3">
				<code>form-vertical well</code>
				<form class="form-vertical well">
					<p><label for="input">type="text"</label><input placeholder="text" type="text" id="input" /></p>
					<p><label for="radio1">Radio1</label><input type="radio" name="radio" id="radio1" /></p>
					<p><label for="radio2">Radio2</label><input type="radio" name="radio" id="radio2" /></p>
					<p><label for="checkbox">checkbox</label><input type="checkbox" name="checkbox" id="checkbox" /></p>
					<p><label for="select">Select</label><select id="select"><option>test</option><option>test</option><option>test</option></select></p>
					<p><label for="selectm">Select Multiple</label><select multiple="multiple" id="selectm"><option>test</option><option>test</option><option>test</option></select></p>
					<p><label for="textarea">Textarea</label><textarea id="textarea" placeholder="textarea"></textarea></p>
					<hr />
					<p><input type="submit" value="Submit"> <button>Button</button></p>
				</form>
			</div>
			<div class="span3">
				<code>form-horizontal well</code>
				<form class="form-horizontal well">
					<p><label for="input-h">type="text"</label><input placeholder="text" type="text" id="input-h" /></p>
					<p><label for="radio1-h">Radio1</label><input type="radio" name="radio" id="radio1-h" /></p>
					<p><label for="radio2-h">Radio2</label><input type="radio" name="radio" id="radio2-h" /></p>
					<p><label for="checkbox-h">checkbox</label><input type="checkbox" name="checkbox" id="checkbox-h" /></p>
					<p><label for="select-h">Select</label><select id="select-h"><option>test</option><option>test</option><option>test</option></select></p>
					<p><label for="selectm-h">Select Multiple</label><select multiple="multiple" id="selectm-h"><option>test</option><option>test</option><option>test</option></select></p>
					<p><label for="textarea-h">Textarea</label><textarea id="textarea-h" placeholder="textarea"></textarea></p>
					<hr />
					<p><input type="submit" value="Submit"> <button>Button</button></p>
				</form>
			</div>
			<div class="span4">
				<code>form-inline well</code>
				<form class="form-inline well">
					<input type="text" class="input-small" placeholder="Email">
					<input type="checkbox" />
					<label class="checkbox">Remember me</label>
					
					<input type="submit" value="Sign in" />
				</form>
			</div>
		</section>
		<section class="row">
			<h3 class="span2">Classes de formulaire</h3>
			<div class="span3">
				<code>&lt;span class="required">*&lt;span></code><br />
				<label for="required">Nom<span class="required" title="Obligatoire">*</span>:</label> <input type="text" id="required" placeholder="Obligatoire" />
			</div>
		</section>

		<section class="row">
			<h3 class="span2">
				Textarea avec décompte
			</h3>
			<div class="span2">
				<textarea class="t-countdown" maxlength="25" placeholder="Essayez moi !" rows="5"></textarea>
			</div>
			<div class="span3">
				<pre class="prettyprint">
&lt;textarea  maxlength="25" placeholder="Essayez moi !">&lt;/>
<hr />$('textarea').textarea_countdown();</pre>
			</div>
			
			<div class="span2">
				<textarea class="t-countdown" maxlength="25" data-state="false" rows="5">Valeur par défaut</textarea>
			</div>
			<div class="span3">
				<pre class="prettyprint">
&lt;textarea  maxlength="25" data-state="false">Valeur par défaut&lt;/>
<hr />$('textarea').textarea_countdown();</pre>
			</div>
		</section>
		
		<h1>Ajax</h1>
		<section class="row">
			<div class="span2">
				<h3>Loader</h3>
			</div>
			<div class="span2">
				<code>Ajax loader</code> <input type="submit" id="submit_ajax_loader" value="Do something ajax" />
			</div>
			<div class="span8">
				
				<p>Désactive lors appel Ajax <code>$('.btn-loader').disableOnAjax();</code> <input type="submit" class="btn-loader" value="Disable on ajax" /></p>
				<p>Désactive lors appel Ajax <code>$('.btn-loader-img').disableOnAjax({'img': 'img/ajax-loader.gif'});</code> <input type="submit" class="btn-loader-img" value="Disable" /></p>
				<p>Désactive lors appel Ajax <code>$('.btn-loader-icon').disableOnAjax({'icon': true});</code> <input type="submit" class="btn-loader-icon" value="val()" /><button class="btn-loader-icon">html()</button><span class="btn-loader-icon clickable white">html()</span></p>
			</div>
		</section>
		
		<h1>Notifications Flash</h1>
		<section class="row">
			<div class="span4">
				<p><button id="btn-flash-1">Notification Basique</button></p>
				<pre class="prettyprint">
$('#btn-flash-1').click(function() {
   $.flash.add('Ma simple notification');
});</pre>
			</div>
			<div class="span4">
				<p><button id="btn-flash-2">Notification Avancée</button></p>
				<pre class="prettyprint">
$.flash.add({
   text: "Notif <strong>modifiée</strong>",
   classes: 'well error shadow-outset',
   delay: 4000,
   effect: 'drop',
   effect_options: {'direction': 'right'},
   speed_in: 'fast',
   speed_out: 'slow',
   cross_close: true
});</pre>
			</div>
			<div class="span4">
				<h3>Autres configurations</h3>
				<pre class="prettyprint">
// changer position des notifs
//$.flash.[top|right|left|bottom]
$.flash.left = '5px'; $.flash.bottom = '20px';
// changer les paramètres pour toutes les notifs
// $.flash.[paramètre]
$.flash.delay = 5000;
$.flash.cross_close = true; // etc...</pre>
			</div>
		</section>
		
		
		
		<hr />
		<section class="row">
			<div class="span12">
				<h1>Test d'écran</h1>
				<h4>Visible on...</h4>
				<ul class="responsive-utilities-test">
					<li>Phone<span class="visible-phone">&#10004; Phone</span></li>
					<li>Tablet<span class="visible-tablet">&#10004; Tablet</span></li>
					<li>Desktop<span class="visible-desktop">&#10004; Desktop</span></li>
				</ul>
				<h4>Hidden on...</h4>
				<ul class="responsive-utilities-test hidden-on">
					<li>Phone<span class="hidden-phone">&#10004; Phone</span></li>
					<li>Tablet<span class="hidden-tablet">&#10004; Tablet</span></li>
					<li>Desktop<span class="hidden-desktop">&#10004; Desktop</span></li>
				</ul>
			</div>
		</section>
		
		<hr />
		<section id="modif">
			<form action="" method="post" class="form-vertical">
				<div class="row">
					<div class="span2">
						<label for="color_grey">Couleur grise:</label><input type="color" id="color_grey" name="color_grey" value="#8D8D8D" placeholder="#8D8D8D" />
						<label for="color_green">Couleur verte:</label><input type="color" id="color_green" name="color_green" value="#4B994A" placeholder="#4B994A" />
					</div>
					<div class="span2">
						<label for="color_black">Couleur noire:</label><input type="color" id="color_black" name="color_black" value="#000000" placeholder="#000000" />
						<label for="color_blue">Couleur bleue:</label><input type="color" id="color_blue" name="color_blue" value="#3D62D3" placeholder="#3D62D3" />
					</div>
					<div class="span2">
						<label for="color_white">Couleur rouge:</label><input type="color" id="color_white" name="color_white" value="#FFFFFF" placeholder="#FFFFFF" />
						<label for="color_violet">Couleur violette:</label><input type="color" id="color_violet" name="color_violet" value="#9B3BCC" placeholder="#9B3BCC" />
					</div>
					<div class="span2">
						<label for="color_red">Couleur rouge:</label><input type="color" id="color_red" name="color_red" value="#D13434" placeholder="#D13434" />
						<label for="color_pink">Couleur rose:</label><input type="color" id="color_pink" name="color_pink" value="#EF2B8A" placeholder="#EF2B8A" />
					</div>
					<div class="span2">
						<label for="color_orange">Couleur orange:</label><input type="color" id="color_orange" name="color_orange" value="#FF8C00" placeholder="#FF8C00" />
					</div>
					<div class="span2">
						<label for="color_yellow">Couleur jaune:</label><input type="color" id="color_yellow" name="color_yellow" value="#EDC309" placeholder="#EDC309" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="color_success">Couleur success:</label><input type="color" id="color_success" name="color_success" value="#4B994A" placeholder="#4B994A" />
					</div>
					<div class="span2">
						<label for="color_error">Couleur error:</label><input type="color" id="color_error" name="color_error" value="#D13434" placeholder="#D13434" />
					</div>
					<div class="span2">
						<label for="color_highlight">Couleur highlight:</label><input type="color" id="color_highlight" name="color_highlight" value="#3D62D3" placeholder="#3D62D3" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="well_border_radius">.well radius:</label><input type="text" id="well_border_radius" name="well_border_radius" value="5px" placeholder="5px" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="small_size">.small:</label><input type="text" id="small_size" name="small_size" value="0.8em" placeholder="0.8em" />
					</div>
					<div class="span2">
						<label for="big_size">.big:</label><input type="text" id="big_size" name="big_size" value="1.2em" placeholder="1.2em" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="table_strip_color">Couleur strip table:</label><input type="color" id="table_strip_color" name="table_strip_color" value="#EEEEEE" placeholder="#EEEEEE" />
					</div>
					<div class="span2">
						<label for="table_border_color">Couleur bordure table:</label><input type="color" id="table_strip_color" name="table_border_color" value="#C0C0C0" placeholder="#C0C0C0" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="fade_opacity">.fade Opacité:</label><input type="number" min="0" max="1" step="0.1" id="fade_opacity" name="fade_opacity" value="0.4" placeholder="0.4" />
					</div>
					<div class="span2">
						<label for="fade_transition_speed">.fade vitesse transition:</label><input type="text" id="fade_transition_speed" name="fade_transition_speed" value="0.2s" placeholder="0.2s" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="label_radius">.label radius:</label><input type="text" id="label_radius" name="label_radius" value="3px" placeholder="3px" />
					</div>
					<div class="span2">
						<label for="label_padding">.label padding:</label><input type="text" id="label_padding" name="label_padding" value="1px 4px 2px" placeholder="1px 4px 2px" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="badge_radius">.badge radius:</label><input type="text" id="badge_radius" name="badge_radius" value="9px" placeholder="9px" />
					</div>
					<div class="span2">
						<label for="badge_padding">.badge padding:</label><input type="text" id="badge_padding" name="badge_padding" value="1px 9px 2px" placeholder="1px 9px 2px" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="label_badge_shadow_size">.label/.badge taille de l'ombre:</label><input type="text" id="label_badge_shadow_size" name="label_badge_shadow_size" value="0px 0px 4px 0px" placeholder="0px 0px 4px 0px" />
					</div>
					<div class="span2">
						<label for="label_badge_shadow_speed">.label/.badge vitesse transition de l'ombre:</label><input type="text" id="label_badge_shadow_speed" name="label_badge_shadow_speed" value="0.2s" placeholder="0.2s" />
					</div>
				</div>
				<div class="row">
					<div class="span2">
						<label for="input_text_border_radius">input/textarea radius:</label><input type="text" id="input_text_border_radius" name="input_text_border_radius" value="3px" placeholder="3px" />
					</div>
				</div>
				<div class="row">
					<div class="span12">
						<input type="submit" name="modif" value="Modifier !" />
						<input type="reset" name="reset" value="Réinitialiser les valeurs" />
						<span class="buttonset">
							<label style="display: inline-block;" for="type_sortie_extend">Etendue</label><input type="radio" name="type_sortie" id="type_sortie_extend" value="extend" checked="checked" /><label style="display: inline-block;" for="type_sortie_compress">Compressée</label><input type="radio" name="type_sortie" id="type_sortie_compress" value="compress" />
						</span>
					</div>
				</div>
			</form>
			<p id="scss-error" class="error"></p>
		</section>

		<footer>
			<p>&copy; Templates 2012</p>
		</footer>
	</div> <!-- /container -->
	

	

<!--<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>-->
<script type="text/javascript" src="js/jquery-dev.js"></script>
<script type="text/javascript" src="http://cdn.bitbucket.org/shekharpro/google_code_prettify/downloads/prettify.js"></script>
<script type="text/javascript" src="js/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</body>
</html>