<?php

$elements = array(	'<div>[div 1]</div>',
					'<div>[div 2]</div>',
					'<div>[div 3]</div>',
					'<div>[div 4]</div>',
					'<div>[div 5]</div>',
					'<div>[div 6]</div>',
					'<div>[div 7]</div>',
					'<div>[div 8]</div>',
					'<div>[div 9]</div>',
					'<div>[div 10]</div>',
					'<div>[div 11]</div>',
					'<div>[div 12]</div>',
					'<div>[div 13]</div>',
					'<div>[div 14]</div>',
					'<div>[div 15]</div>',
					'<div>[div 16]</div>',
					'<div>[div 17]</div>',
					'<div>[div 18]</div>',
					'<div>[div 19]</div>',
					'<div>[div 20]</div>',
					'<div>[div 21]</div>',
					'<div>[div 22]</div>',
					'<div>[div 23]</div>',
					'<div>[div 24]</div>',
					'<div>[div 25]</div>',
					'');

sleep(1);

if(!empty($_POST['nb_by_pages']) || !empty($_POST['current_page'])) {
	
	$current_page	= intval($_POST['current_page'] - 1);
	$nb_by_pages	= intval($_POST['nb_by_pages']);
	$json			= ($_POST['json'] == 'true' ? true : false);
	
	$i = $current_page * $nb_by_pages;
	$stop = $i + $nb_by_pages;
	if($json) {
		$datas = array();
		for($i; $i < $stop; $i++) {
			$datas[] = $elements[$i];
		}
		
		$retour = array(	'error' => false,
							'datas' => $datas);
		echo json_encode($retour);
	}
	else {
		$datas = '';
		for($i; $i < $stop; $i++) {
			$datas .= $elements[$i];
		}

		echo $datas;
	}
	
}
else {
	if($json == true) {
		echo json_encode(array('error' => true));
	}
	else {
		echo '<span class="error">Erreur de paramètres</span>';
	}
}

?>
