/**
 *	core.js
 *	V0.1.4.1
 *	- Fix dropdown Opera/IE
**/

/* Datepicker en FR */
$.datepicker.regional['fr'] = {
	closeText: 'Fermer',
	prevText: 'Préc',
	nextText: 'Suiv',
	currentText: 'Courant',
	monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
	'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
	monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
	'Jul','Aoû','Sep','Oct','Nov','Déc'],
	dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
	dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
	dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['fr']);


/**
 * Plug-in Jquery >> Confirm
 * V0.4.1
 * - fix doublon
 * 
 * Permet de "simuler" un confirm js avec Jquery UI Dialog
 * data-confirm-title						=> titre du dialog
 * data-confirm-message						=> message html du dialog
 * data-confirm-dialogClass					=> class css rajoutée au dialog
 * data-confirm-width						=> largeur du dialog
 * data-confirm-height						=> hauteur du dialog
 * data-confirm-button-ok					=> texte du bouton ok
 * data-confirm-button-cancel				=> texte du bouton cancel
 * data-confirm-button-ok-icon-primary		=> icon primaire du bouton ok
 * data-confirm-button-cancel-icon-primary	=> icon primaire du bouton cancel
 * data-confirm-button-ok-icon-secondary	=> icon secondaire du bouton ok
 * data-confirm-button-cancel-icon-secondary=> icon secondaire du bouton cancel
**/ 
(function($) {
	$.fn.confirm = function(options)
	{ 

		return this.each(function()
		{

			var defauts =
			{
				"title": "Confirmer",
				"message": "Etes-vous vraiment sûr de vouloir effectuer cette action ?",
				"dialogClass": "",
				"width": 300,
				"height": "auto",
				"button_ok": "Ok",
				"button_cancel": "Annuler",
				"button_ok_icon_primary": {},
				"button_ok_icon_secondary": {},
				"button_cancel_icon_primary": {},
				"button_cancel_icon_secondary": {},
				"ok": null,
				"cancel": null
			}; 
			var parametres = $.extend(defauts, options);
			
			$(this).click(function(event) {
				event.preventDefault();
				var $targetDialog = $(this);
				var Ok, Cancel, Ok_icons = {}, Cancel_icons = {};
				
				if($targetDialog.attr('data-confirm-title') != undefined) parametres.title = $targetDialog.attr('data-confirm-title');
				if($targetDialog.attr('data-confirm-message') != undefined) parametres.message = $targetDialog.attr('data-confirm-message');
				if($targetDialog.attr('data-confirm-dialogClass') != undefined) parametres.dialogClass = $targetDialog.attr('data-confirm-dialogClass');
				if($targetDialog.attr('data-confirm-width') != undefined) parametres.width = $targetDialog.attr('data-confirm-width');
				if($targetDialog.attr('data-confirm-height') != undefined) parametres.height = $targetDialog.attr('data-confirm-height');
				if($targetDialog.attr('data-confirm-button-ok') != undefined) Ok = $targetDialog.attr('data-confirm-button-ok');
					else Ok = parametres.button_ok;
				if($targetDialog.attr('data-confirm-button-cancel') != undefined) Cancel = $targetDialog.attr('data-confirm-button-cancel');
					else Cancel = parametres.button_cancel;
				
				// icons buttons
				if($targetDialog.attr('data-confirm-button-ok-icon-primary') != undefined) Ok_icons.primary = $targetDialog.attr('data-confirm-button-ok-icon-primary'); // si data-
					else if(!$.isEmptyObject(parametres.button_ok_icon_primary)) Ok_icons.primary = parametres.button_ok_icon_primary; // sinon si attribut js existe
				if($targetDialog.attr('data-confirm-button-cancel-icon-primary') != undefined) Cancel_icons.primary = $targetDialog.attr('data-confirm-button-cancel-icon-primary');
					else if(!$.isEmptyObject(parametres.button_cancel_icon_primary)) Cancel_icons.primary = parametres.button_cancel_icon_primary;
				if($targetDialog.attr('data-confirm-button-ok-icon-secondary') != undefined) Ok_icons.secondary = $targetDialog.attr('data-confirm-button-ok-icon-secondary');
					else if(!$.isEmptyObject(parametres.button_ok_icon_secondary)) Ok_icons.secondary = parametres.button_ok_icon_secondary;
				if($targetDialog.attr('data-confirm-button-cancel-icon-secondary') != undefined) Cancel_icons.secondary = $targetDialog.attr('data-confirm-button-cancel-icon-secondary');
					else if(!$.isEmptyObject(parametres.button_cancel_icon_secondary)) Cancel_icons.secondary = parametres.button_cancel_icon_secondary;
				
				$('<div id="dialog-confirm" class="hide">'+parametres.message+'</div>').appendTo('body').dialog({
					autoOpen: false,
					resizable: false,
					draggable: false,
					dialogClass: parametres.dialogClass,
					width: parametres.width,
					height: parametres.height,
					title: parametres.title,
					modal: true,
					closeText: 'Fermer',
					buttons: [
						{
							text: Ok,
							icons: Ok_icons,
							click: function() {
								$(this).dialog("destroy").remove();
								if(parametres.ok) {
									parametres.ok();
								}
								else {

									if($targetDialog.is('a'))	window.location.href = $targetDialog.attr('href');
									else $targetDialog.unbind('click').bind('click').click();
									/*else return true;*/
								}
							}
						},
						{
							text: Cancel,
							icons: Cancel_icons,
							click: function() {
								$(this).dialog("destroy").remove();
								if(parametres.cancel) {
									parametres.cancel();
								}
								/*else return false;*/
							}
						}
					]
				}).dialog("open");
			});
		});		
	};
})(jQuery);

/**
 * Plug-in Jquery >> Toggle
 * V0.2.1
 * 
 * Toggle un element du DOM avec data
 * data-toggle	=> id de l'element a afficher/cacher
 * data-anim	=> type de l'animation [*, slide, fade]
 * data-visible => text à placer lorsque l'élément est cliqué et devient visible
 * data-hidden	=> text à placer lorsque l'élément est cliqué et devient caché
**/ 
(function($) {
	$.fn.toggleIt = function()
	{ 

		return this.each(function()
		{
			$(this).click(function(event) {
				event.preventDefault();

				var $this = $(this);
				var $elem = $('#'+$(this).attr('data-toggle'));
				if($this.attr('data-toggle') != undefined) {
					if($this.attr('data-anim') == 'slide')	$elem.stop(true).slideToggle(function() {
						if($this.attr('data-visible') != undefined && $this.attr('data-hidden') != undefined) {

							if($elem.is(':visible'))	{$this.html($this.attr('data-visible'));}
							else						{$this.html($this.attr('data-hidden'));}
						}
					});
					else if($this.attr('data-anim') == 'fade')	$elem.stop(true).fadeToggle(function() {
						if($this.attr('data-visible') != undefined && $this.attr('data-hidden') != undefined) {

							if($elem.is(':visible'))	{$this.html($this.attr('data-visible'));}
							else						{$this.html($this.attr('data-hidden'));}
						}
					});
					else								$elem.stop(true).toggle($this.attr('data-anim'), function() {
						if($this.attr('data-visible') != undefined && $this.attr('data-hidden') != undefined) {

							if($elem.is(':visible'))	{$this.html($this.attr('data-visible'));}
							else						{$this.html($this.attr('data-hidden'));}
						}
					});
				}
					
			});
		});		
	};
})(jQuery);

/**
 * Plug-in Jquery >> Dropdown
 * V0.3.1
 * - Fix Opera/IE
 * 
 * Mini Dropdown
 * data-dropdown	=> id de l'element a afficher/cacher
 * data-my			=> position du dropdown
 * data-at			=> position par rapport au parent
 * data-effect		=> type de l'animation [*, slide, fade]
 * data-direction	=> direction de l'effet (silide)
 * data-duration	=> vitesse de l'effet
 * data-easing		=> easing UI
**/ 
(function($) {
	$.fn.dropdown = function(options)
	{
		function position($dropdown, parametres) {
			$('#'+$dropdown.attr('data-dropdown')).css('position', 'absolute').position({
				of: $dropdown,
				my: parametres.my,
				at: parametres.at,
				offset: 0,
				collision: 'none none'
			});
		}

		return this.each(function()
		{
			var defauts =
			{
				'my' : 'left top',
				'at' : 'left bottom',
				'effect'	: 'slide',
				'direction'	: 'up',
				'duration'	: 'normal',
				'easing'	: 'swing'
			}; 
			var parametres = $.extend(defauts, options);

			var $dropdown = $(this);
			var $target = $('#'+$dropdown.attr('data-dropdown'));
			
			if($dropdown.attr('data-dropdown-my') != undefined) parametres.my = $dropdown.attr('data-dropdown-my');
			if($dropdown.attr('data-dropdown-at') != undefined) parametres.at = $dropdown.attr('data-dropdown-at');
			if($dropdown.attr('data-dropdown-effect') != undefined) parametres.effect = $dropdown.attr('data-dropdown-effect');
			if($dropdown.attr('data-dropdown-direction') != undefined) parametres.direction = $dropdown.attr('data-dropdown-direction');
			if($dropdown.attr('data-dropdown-duration') != undefined) parametres.duration = $dropdown.attr('data-dropdown-duration');
			if($dropdown.attr('data-dropdown-easing') != undefined) parametres.easing = $dropdown.attr('data-dropdown-easing');
			
			$target.css({'position' : 'absolute'}).position({
				of: $dropdown,
				my: parametres.my,
				at: parametres.at,
				offset: 0,
				collision: 'none none'
			}).hide();

			$dropdown.click( function() {
				if($target.is(':visible')) { $target.stop(true).toggle(parametres.effect, {'direction': parametres.direction, 'easing': parametres.easing}, parametres.duration); }
				else { $target.show(); position($dropdown, parametres); $target.hide(0); $target.stop(true, true).toggle(parametres.effect, {'direction': parametres.direction, 'easing': parametres.easing}, parametres.duration); }
			})
			.blur( function(e) {
				if (/msie|MSIE|opera|OPERA|Opera|Presto/.test(navigator.userAgent)) { e.preventBubble(); } // fix opera/IE
				$target.stop(true, true).hide(parametres.effect, {'direction': parametres.direction, 'easing': parametres.easing}, parametres.duration);
			});
			
			
			if (/msie|MSIE|opera|OPERA|Opera|Presto/.test(navigator.userAgent)) {
				$target.on('click' ,function() {
					$target.stop(true, true).hide(parametres.effect, {'direction': parametres.direction, 'easing': parametres.easing}, parametres.duration);
				});
			} // fix opera/IE
			
			$(window).on('resize', function() {$target.show(0); position($dropdown, parametres); $target.hide(0);}); // replace les dropdown lors d'un resize de fenetre
		});		
	};
})(jQuery);



/**
 * Plug-in Jquery >> Textarea-Countdown
 * V1.1
 * - Ajout event input
 * - Ajout fallback maxlength
 * 
 * counteur de caractères avec param maxlength
 * data-state	=> ajoute des etats [error/highlight/success] en fonction du nombre de caractères restants
**/ 
(function($) {
	$.fn.textarea_countdown = function(options)
	{	
		return this.each(function()
		{
			var defauts =
			{
				'maxlength'	: 255,
				'state'		: true
			}; 
			var parametres = $.extend(defauts, options);

			var $textarea = $(this);
			
			if($textarea.attr('maxlength')) parametres.maxlength = $textarea.attr('maxlength');
			if($textarea.attr('data-state') != undefined) parametres.state = $textarea.attr('data-state');
			
			var countdown = parametres.maxlength - $textarea.val().length;
			
			$textarea.wrap('<div class="textarea-wrapper" style="display: '+ $textarea.css('display') +'; width: '+ $textarea.outerWidth() +'px" />');
			$textarea.after('<div class="textarea-countdown-text" />');
			
			var $countdown_text = $textarea.next();
			$countdown_text.html('Il reste <strong class="textarea-countdown">'+ countdown +'</strong> caractère<span class="textarea-plural">'+(countdown > 1 ? 's</span>' : '</span>'));
			if($textarea.attr('id') != '') $countdown_text.wrap('<label for="'+$textarea.attr('id')+'" />');
			
			var $countdown = $countdown_text.children('.textarea-countdown');
			var $plural = $countdown_text.children('.textarea-plural');
			
			$textarea
				.on('keydown keyup click paste change input', function() {
					var nb_car = $textarea.val().length;
					countdown = parametres.maxlength - nb_car;
					
					if(countdown < 0) { countdown = 0; $textarea.val($textarea.val().substr(0, nb_car - 1)); } // fallback IE/Opera(Presto)
					
					$countdown.html(countdown);
					if(countdown > 1 && $plural.html() != 's') {
						$countdown_text.children('.textarea-plural').html('s');
					}
					else if(countdown <= 0 && $plural.html() == 's') {
						$countdown_text.children('.textarea-plural').empty();
					}
					
					if(parametres.state != "false") {
						if(countdown <= 10 && !$countdown.hasClass('error hightlight')) {
							$countdown.removeClass('highlight').addClass('error');
						}
						else if(countdown <= 15 && !$countdown.hasClass('error highlight')) {
							$countdown.removeClass('error').addClass('highlight');
						}
						else if($countdown.hasClass('error') || $countdown.hasClass('highlight')) {
							$countdown.removeClass('error highlight');
						}
					}
				})
		});
	};
})(jQuery);





/**
 * Plug-in Jquery >> Carousel
 * V0.3
 * - suppression du trigger ajout fonction en paramètre
 * - easing
 * 
 * Carousel sur élément du DOM
 * 
**/ 
(function($) {
	$.fn.carousel = function(options)
	{
		var defauts =
		{
			'vertical'		: false,
			'nb_shown'		: 1,
			'nb_move'		: 1,
			'loop'			: false,
			'can_disable'	: true,
			'duration'		: 'normal',
			'easing'		: 'linear', /* voir http://jqueryui.com/demos/effect/easing.html */
			'autoSlide'		: false,
			'autoSlide_duration' : 6000,
			'onslide'		: null,
			'onbeforeslide'	: null,
			'onafterslide'	: null
		}; 
		var parametres = $.extend(defauts, options);
			
		return this.each(function()
		{
			var $this = $(this);

			// Calcul préalables :
			// Element de référence pour la zone de visualisation (ici le premier item)
			var Reference = $this.children(':first');
			// Nombre d'éléments de la liste
			var NbElement = $this.children().length;
			// Initialisation du compteur
			var Cpt = 0;

			// Ciblage de la bande de diapositives
			if(!parametres.vertical) {
				$this
					.css("width", (Reference.outerWidth() * NbElement) ) // Application d'une largeur à la bande de diapositive afin de conserver une structrure horizontale
					.attr('data-current-num', 0) // Ajout du data permettant de getter le numéro du slide
					.wrap('<div class="carousel-conteneur" />'); // ajout du conteneur
				
				var $carousel_conteneur = $this.parent();

				$carousel_conteneur
					.wrap('<div class="carousel-block" />')
					.before('<div class="carousel-slide-right"><button></button></div>')
					.before('<div class="carousel-slide-left"><button></button></div>');
			}
			else {
				$this
					.css("height", (Reference.outerHeight() * NbElement) ) // Application d'une largeur à la bande de diapositive afin de conserver une structrure horizontale
					.attr('data-current-num', 0) // Ajout du data permettant de getter le numéro du slide
					.wrap('<div class="carousel-conteneur" />'); // ajout du conteneur
				
				var $carousel_conteneur = $this.parent();

				$carousel_conteneur
					.wrap('<div class="carousel-block vertical" />')
					.after('<div class="carousel-slide-right"><button></button></div>')
					.before('<div class="carousel-slide-left"><button></button></div>');
			}
			
			var $carousel = $carousel_conteneur.parent();
			var $pagination_right = $carousel.children('.carousel-slide-right');
			var $pagination_left = $carousel.children('.carousel-slide-left');
			
			if(!parametres.vertical) {
				// Ciblage de la zone de visualisation
				$carousel_conteneur
					// Application de la largeur d'une seule diapositive
					.width(  Reference.outerWidth() * parametres.nb_shown  ) /* * nb_shown car nb_shown table montrées */
					// Application de la hauteur d'une seule diapositive
					.height( Reference.outerHeight() )
					// Blocage des débordements
					.css("overflow", "hidden");

				$carousel.width($carousel_conteneur.outerWidth()+$pagination_right.outerWidth()+$pagination_left.outerWidth()); // taille du block de carousel

				$pagination_right.height($carousel_conteneur.outerHeight()).children('button').button({icons: {primary: "ui-icon-carat-1-e"}, text: false}).removeClass('ui-corner-all').addClass('ui-corner-right');
				$pagination_left.height($carousel_conteneur.outerHeight()).children('button').button({icons: {primary: "ui-icon-carat-1-w"}, text: false}).removeClass('ui-corner-all').addClass('ui-corner-left'); // switchClass('ui-corner-all','ui-corner-left') Jquery UI
			}
			else {
				$carousel_conteneur
					.width(  Reference.outerWidth() )
					.height( Reference.outerHeight() * parametres.nb_shown )
					.css("overflow", "hidden");

				$carousel.height($carousel_conteneur.outerHeight()+$pagination_right.outerHeight()+$pagination_left.outerHeight());

				$pagination_right.width($carousel_conteneur.outerWidth()).children('button').button({icons: {primary: "ui-icon-carat-1-s"}, text: false}).removeClass('ui-corner-all').addClass('ui-corner-bottom');
				$pagination_left.width($carousel_conteneur.outerWidth()).children('button').button({icons: {primary: "ui-icon-carat-1-n"}, text: false}).removeClass('ui-corner-all').addClass('ui-corner-top'); // switchClass('ui-corner-all','ui-corner-left') Jquery UI
				
			}
			
			if(parametres.loop || parametres.autoSlide) {
				parametres.can_disable = false;
			}
			if(parametres.autoSlide) { // si autoslide
				parametres.loop = true;
				
				var interval = setInterval(function() {
						$pagination_right.click();
					}, parametres.autoSlide_duration);
				
				$carousel.mouseenter(function() {
					interval = clearInterval(interval);
				})
				.mouseleave( function() {
					interval = setInterval(function() {
						$pagination_right.click();
					}, parametres.autoSlide_duration);
				});

			}
			
			// Actions de navigation
			// Clic sur le bouton "Suivant"
			$pagination_right.click(function() {
				if(parametres.onbeforeslide) parametres.onbeforeslide();
				
				if(Cpt < (NbElement - parametres.nb_shown) ) { // Si le compteur est inférieur au nombre de diaposives moins 1
					Cpt = Cpt + parametres.nb_move; // Ajout nb_move au compteur (nous allons sur la diapositive suivante)
				}
				else if(Cpt >= (NbElement - parametres.nb_shown) && parametres.loop) {
					Cpt = 0; // retour au premier
				}
				// Maj data
				$this.attr('data-current-num', Cpt);
				if(parametres.onslide) parametres.onslide();
				
				var move = {marginLeft : - (Reference.outerWidth() * Cpt)};
				if(parametres.vertical) move = {marginTop : - (Reference.outerHeight() * Cpt)};
				
				// Mouvement du carousel en arrière-plan
				$this.stop(true, true).animate(move, parametres.duration, parametres.easing, function() {
						if(parametres.onafterslide) parametres.onafterslide();
				});
				//$( "#slider" ).slider( "value", Cpt );
				$this.trigger('carousel', 'right');
			});
			// Action du bouton "Précédent"
			$pagination_left.click(function() {
				if(parametres.onbeforeslide) parametres.onbeforeslide();

				if(Cpt > 0) { // Si le compteur est supérieur à zéro
					// Soustraction -1 au compteur (nous allons sur la diapositive précédente)
					Cpt = Cpt - parametres.nb_move;
				}
				else if(Cpt <= 0 && parametres.loop) {
					Cpt = NbElement - parametres.nb_shown; // avancement au dernier
				}
				// Maj data
				$this.attr('data-current-num', Cpt);

				if(parametres.onslide) parametres.onslide();
				
				var move = {marginLeft : - (Reference.outerWidth() * Cpt)};
				if(parametres.vertical) move = {marginTop : - (Reference.outerHeight() * Cpt)};
				// Mouvement du carousel en arrière-plan
				$this.stop(true, true).animate(move, parametres.duration, parametres.easing, function() {
					if(parametres.onafterslide) parametres.onafterslide();
				});
				//$( "#slider" ).slider( "value", Cpt );
				
				$this.trigger('carousel', 'left');
			});
			
			if(parametres.can_disable) {
				if($pagination_left.children().hasClass('ui-button')) {
					$pagination_left.children().button('disable').prop('disabled', true);
				}
				else {
					$pagination_left.children().prop('disabled', true);
				}
				$this.on('carousel', function(event, direction) {

						if($pagination_left.children().hasClass('ui-button')) {
							$pagination_right.children().button('enable').prop('disabled', false);
							$pagination_left.children().button('enable').prop('disabled', false);
						}
						else {
							$pagination_right.children().prop('disabled', false);
							$pagination_left.children().prop('disabled', false);
						}
					
					if(Cpt <= 0) {
						if($pagination_left.children().hasClass('ui-button')) {
							$pagination_left.children().button('disable').prop('disabled', true);
						}
						else {
							$pagination_left.children().prop('disabled', true);
						}
					}
					else if(Cpt >= NbElement - parametres.nb_shown) {
						if($pagination_left.children().hasClass('ui-button')) {
							$pagination_right.children().button('disable').prop('disabled', true);
						}
						else {
							$pagination_right.children().prop('disabled', true);
						}
					}
				});
			}
			
			$this.on('slideTo', function(event, new_page) {
				
				new_page = parseInt(new_page-1);
				var this_direction = (Cpt > new_page ? 'left' : 'right');
				if(parametres.onbeforeslide) parametres.onbeforeslide();
				
				Cpt = new_page - (new_page%parametres.nb_shown); // mise à niveau du compteur
				
				if(Cpt > 0 && Cpt < NbElement) {
					Cpt = new_page - (new_page%parametres.nb_shown);
				}
				else {
					Cpt = 0;
					this_direction = 'left';
				}
				$this.attr('data-current-num', Cpt);

				if(parametres.onslide) parametres.onslide();
				
				var move = {marginLeft : - (Reference.outerWidth() * Cpt)};
				if(parametres.vertical) move = {marginTop : - (Reference.outerHeight() * Cpt)};

				$this.stop(true, true).animate(move, parametres.duration, parametres.easing, function() {
					if(parametres.onafterslide) parametres.onafterslide();
				});
				
				$this.trigger('carousel', this_direction);
			});
		});		
	};
})(jQuery);



/**
 * Plug-in Jquery >> Pagination
 * V0.1
 * 
 * Pagination
 * 
**/ 
(function($) {
	$.fn.pagination = function(options)
	{
		var defauts =
		{
			'target'		: null,
			'nb_by_pages'	: 10,
			'classes'		: 'pagination',
			'effect'		: 'fade',
			'speed_out'		: 'fast',
			'speed_in'		: 'slow',
			'effect_options': {},
			'ajax'			: { 
				'url':null,
				'datas':null,
				'type':'post',
				'nb_total': null,
				'json':null,
				'success':null,
				'error':null
			}, // type: [post, get], url: string, nb_total: int, success: fnc, error: fnc, json: [true, false]
			// Carousel héritage
			'nb_shown'		: 5,
			'nb_move'		: 1,
			/*'can_disable'	: true,*/
			'duration'		: 'fast',
			'easing'		: 'linear', /* voir http://jqueryui.com/demos/effect/easing.html */
			'onslide'		: null,
			'onbeforeslide'	: null,
			'onafterslide'	: null
		}; 
		var parametres = $.extend(defauts, options);
		
		var $target = $('#'+parametres.target);
		
		if($target.length > 0) { // si target existe
			return this.each(function()
			{
				var $pagination = $(this);

				if(parametres.ajax.url != null) {

					var num_pages = Math.ceil(parametres.ajax.nb_total / parametres.nb_by_pages);
					var ajax = parametres.ajax;
					ajax.url = (parametres.ajax.url != undefined ? parametres.ajax.url : null );
					ajax.datas = (parametres.ajax.datas != undefined ? parametres.ajax.datas : {} );
					ajax.type = (parametres.ajax.type != undefined ? parametres.ajax.type : 'post' );
					ajax.nb_total = (parametres.ajax.nb_total != undefined ? parametres.ajax.nb_total : null );
					ajax.json = (parametres.ajax.json != undefined ? (parametres.ajax.json == 'false' ? false : true) : false );
					ajax.success = (parametres.ajax.success != undefined ? parametres.ajax.success : 
						function(data) {
							if(ajax.json) {
								if(data.error) {
									$target.html('Erreur, vérifiez les attributs Ajax').stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_in);
								}
								else {
									//data.datas.each(function (that) { $target.append(that) });
									//for (var that in data.datas) {$target.append(that);}
									$.each(data.datas, function(key, value){
										$target.append(value);
									});

									$target.stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_in);
								}
							}
							else $target.html(data).stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_in);
						} );
					ajax.error = (ajax.error != undefined ? ajax.error : 
						function(jqXHR, textStatus) {
							$target.html('Erreur :' + textStatus);
						}
					);

				}
				else {
					$target.after($target.clone().attr('id',parametres.target+'-clone').hide());
					var $target_clone = $('#'+parametres.target+'-clone');
					
					var num_pages = Math.ceil($target.children().length / parametres.nb_by_pages);
				}
				
				for(var i=1; i <= num_pages; i++ ) {
					$pagination.append('<button data-num="'+i+'" class="'+parametres.classes+'">'+i+'</button>');
				}
				
				$pagination
				.addClass('pagination-carousel')
				.carousel({
					nb_shown: parametres.nb_shown,
					nb_move: parametres.nb_move,
					can_disable: parametres.can_disable,
					duration: parametres.duration,
					easing: parametres.easing,
					onslide: parametres.onslide,
					onbeforeslide: parametres.onbeforeslide,
					onafterslide: parametres.onafterslide
				})
				.children(':first').addClass('active');
				
				$pagination.parent().prev().prev().children().button('destroy').replaceWith('<button class="'+parametres.classes+'"><span class="ui-icon ui-icon-carat-1-e"></span></button>');
				$pagination.parent().prev().children().button('destroy').replaceWith('<button class="'+parametres.classes+'" disabled><span class="ui-icon ui-icon-carat-1-w"></span></button>');
				
				if(parametres.ajax.url != null && parametres.ajax.nb_total != null) {
					$.ajax({
						url: ajax.url,
						data: {json: ajax.json, nb_by_pages: parametres.nb_by_pages, current_page: $pagination.find('.active').attr('data-num'), datas: ajax.datas },
						type: ajax.type,
						dataType: (ajax.json ? 'json' : 'html'),
						ifModified: true,
						success: function(data, textStatus, jqXHR) {
							$target.stop(true, true).fadeOut('fast').empty();
							ajax.success(data, textStatus, jqXHR);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$target.stop(true, true).fadeOut('fast').empty();
							ajax.error(jqXHR, textStatus, errorThrown);
						},
						beforeSend: function(jqXHR, settings) {
							$target.stop(true, true).hide(parametres.effect, parametres.effect_options, parametres.speed_out, function() {
								$target.html('<img src="img/ajax-loader.gif" class="loader" alt="Chargement ..." title="Chargement ..." />').stop(true, true).fadeIn('slow');
							});
						}
					});
				}
				else {
					var $first = $target.children().slice(0, parametres.nb_by_pages);
					$target.empty().html($first);
				}
				
				$pagination.children().click(function(event) {
					event.preventDefault();
					
					var $this_page = $(this);

					$pagination.children().removeClass('active');
					$this_page.addClass('active');
					
					if(parametres.ajax.url != null) {
						
						$.ajax({
							url: ajax.url,
							data: {json: ajax.json, nb_by_pages: parametres.nb_by_pages, current_page: $pagination.find('.active').attr('data-num'), datas: ajax.datas },
							type: ajax.type,
							dataType: (ajax.json ? 'json' : 'html'),
							ifModified: true,
							success: function(data, textStatus, jqXHR) {
								$target.stop(true, true).fadeOut('fast', function() {
									$target.empty();
									ajax.success(data, textStatus, jqXHR);
								});
							},
							error: function(jqXHR, textStatus, errorThrown) {
								$target.stop(true, true).fadeOut('fast'), function() {
									$target.empty();
									ajax.error(jqXHR, textStatus, errorThrown);
								}
							},
							beforeSend: function(jqXHR, settings) {
								$target.stop(true, true).hide(parametres.effect, parametres.effect_options, parametres.speed_out, function() {
									$target.html('<img src="img/ajax-loader.gif" class="loader" alt="Chargement ..." title="Chargement ..." />').stop(true, true).fadeIn('slow');
								});
							}
						});
					}
					else {
					/* Compatibilité non UI
					if(parametres.effect == 'fade') {
						$target.stop(true, true).fadeOut(parametres.speed_out, function() {
							$target.empty().html($target_clone.html());
							var num_page = $this_page.html()-1;

							var $selection = $target.children().slice(parseInt(num_page*parametres.nb_by_pages), parseInt(num_page*parametres.nb_by_pages+parametres.nb_by_pages));
							$target.empty().html($selection);
							$target.stop(true, true).fadeIn(parametres.speed_in);
						});
					}
					else {*/
						$target.stop(true, true).hide(parametres.effect, parametres.effect_options, parametres.speed_out, function() {
							$target.empty().html($target_clone.html());
							var num_page = $this_page.html()-1;

							var $selection = $target.children().slice(parseInt(num_page*parametres.nb_by_pages), parseInt(num_page*parametres.nb_by_pages+parametres.nb_by_pages));
							$target.empty().html($selection);
							$target.stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_in);
						});
					//}
					}
				});
			});	
		}
		else {
			return this.each(function()
			{
				$(this).html('<span class="error">Erreur de Sélécteur<br />Vérifier l\'appel de la méthode</span>');
			});	
		}
	};
})(jQuery);


/**
 * Plug-in Jquery >> infiniteScroll
 * V0.2
 * 
 * Scroll infinie avec Ajax
**/ 
(function($) {
   $.fn.infiniteScroll = function(options)
   {
	   var defauts =
	   {
		   'nb_to_show'		: 2,
		   'effect'			: 'fade',
		   'speed_out'		: 'fast',
		   'speed_in'		: 'slow',
		   'effect_options'	: {},
		   'ajax'			: { 
			   'url':null,
			   'datas':null,
			   'type':'post',
			   'nb_total': null,
			   'json':null,
			   'success':null,
			   'error':null
		   }, // type: [post, get], url: string, nb_total: int, success: fnc, error: fnc, json: [true, false]
		   'onbeforeload'	: null,	// avant appel ajax
		   'onafterload'	: null,	// juste après appel ajax
		   'onaftershow'	: null	// après affichage du résultat
	   }; 
	   var parametres = $.extend(defauts, options);

	   if(parametres.ajax.url != null) {
		   return this.each(function()
		   {
			   var $infinite = $(this);
			   var $infinite_loader = $infinite.find($('.infinite-loader')).clone();
			   var $infinite_error = $infinite.find($('.infinite-error')).clone();
			   $infinite.empty();
			   var lock_infinite = true; // verrou empechant l'ajax lors du load

			   var ajax = parametres.ajax;
			   ajax.url = (parametres.ajax.url != undefined ? parametres.ajax.url : null );
			   ajax.datas = (parametres.ajax.datas != undefined ? parametres.ajax.datas : {} );
			   ajax.type = (parametres.ajax.type != undefined ? parametres.ajax.type : 'post' );
			   ajax.nb_total = (parametres.ajax.nb_total != undefined ? parametres.ajax.nb_total : null );
			   ajax.json = (parametres.ajax.json != undefined ? (parametres.ajax.json == 'false' ? false : true) : false );
			   ajax.success = (parametres.ajax.success != undefined ? parametres.ajax.success : 
				   function(data) {
					   if(ajax.json) {
						   if(data.error) {
							   $infinite.find($('.infinite-error')).html('Erreur, vérifiez les attributs Ajax').stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_in);
						   }
						   else {
							   $.each(data.datas, function(key, value){
								   $infinite.append(value);
							   });
						   }
					   }
					   else {
						   $infinite.append(data);
					   }
				   } );
			   ajax.error = (ajax.error != undefined ? ajax.error : 
				   function(jqXHR, textStatus) {

				   }
			   );

			   /* -- Fin paramétrage --  */

			   load(parametres, true);

			   $infinite.on({
					scroll: function(){
						if($infinite.scrollTop() + $infinite.height() >= $infinite.get(0).scrollHeight && !lock_infinite) {
							load(parametres, false);
						}
					},
					mouseenter: function(){
						$("body").css("overflow", "hidden");
						$("body").css("padding-right", parseInt($("body").css("padding-right"))+17+"px");
					},
					mouseleave: function(){
						$("body").css("overflow", "auto");
						$("body").css("padding-right", parseInt($("body").css("padding-right"))-17+"px");
					}
			   });

			   function load(parametres, first) {
				   if(parametres.ajax.url != null) {
					   $.ajax({
						   url: ajax.url,
						   data: {json: ajax.json, nb_to_show: parametres.nb_to_show, current: $infinite.children().length, datas: ajax.datas },
						   type: ajax.type,
						   dataType: (ajax.json ? 'json' : 'html'),
						   ifModified: true,
						   success: function(data, textStatus, jqXHR) {
							   parametres.onafterload;
							   $infinite.find($('.infinite-loader')).stop(true, true).hide(parametres.effect, parametres.effect_options, parametres.speed_out).remove();
							   $infinite.find($('.infinite-error')).remove();
							   ajax.success(data, textStatus, jqXHR);
						   },
						   error: function(jqXHR, textStatus, errorThrown) {
							   parametres.onafterload;
							   $infinite.find($('.infinite-error')).html(textStatus).stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_out).delay(3000).hide(parametres.effect, parametres.effect_options, parametres.speed_out).remove();
							   $infinite.find($('.infinite-loader')).remove();
							   ajax.error(jqXHR, textStatus, errorThrown);
						   },

						   complete: function(jqXHR, textStatus) {
							   lock_infinite = false; // on libere le verrou
							   if(first == true) {$infinite.scrollTop(1);} // remise a zero pour éviter looping
							   parametres.onaftershow;
						   },
						   beforeSend: function(jqXHR, settings) {
							   parametres.onbeforeload;
							   lock_infinite = true; // on met le verrou

							   $infinite_loader.appendTo($infinite);
							   $infinite_error.appendTo($infinite);
							   $infinite.find($('.infinite-loader')).stop(true, true).show(parametres.effect, parametres.effect_options, parametres.speed_out); // montre loader
							   $infinite.find($('.infinite-error')).hide(); // cache error
							   // $infinite.scrollTop($infinite.get(0).scrollHeight); // scroll en bas
						   }
					   });
				   }
			   }

		   });
	   }
   }
})(jQuery);



/**
 * Plug-in Jquery >> Notifications flash
 * V0.1
 * 
 * Crée une notification flash facilement
**/ 
(function($) {
	$.flash = {
		top:	'10px',
		right:	'10px',
		init: function() {
			var css = {};
			if(this.left === undefined) { css.right = this.right; }
			else {css.left = this.left}
			if(this.bottom === undefined) { css.top = this.top; }
			else {css.bottom = this.bottom}
			return $('<div id="notif-flash-wrapper" />').css(css).on('click', '.notif-flash-close', function(e){ e.preventDefault(); $(this).parent('.notif-flash').stop(true, true).fadeOut('fast', function() {$(this).remove();});});
		},
		add: function(params) {
			
				var cross_close = params.cross_close || $.flash.cross_close || false;
			
				var delay = params.delay || $.flash.delay || 8000;
				var speed_in = params.speed_in || $.flash.speed_in || 'slow';
				var speed_out = params.speed_out || $.flash.speed_out || 'normal';
				var classes = params.classes || 'well';
				var text = params.text || params || 'Notification !';
				var effect = params.effect || $.flash.effect || 'fade';
				var effect_options = params.effect_options || $.flash.effect_options || {};
			
			if($('#notif-flash-wrapper').length < 1) {$('body').prepend(this.init())};

			var $notif = $('<p class="notif-flash '+classes+'" />').html(text).delay(1).show(effect, effect_options, speed_in).delay(delay).hide(effect, effect_options, speed_out, function() {$(this).remove();});
			if(cross_close) $notif.prepend($('<a href="" class="notif-flash-close ui-icon ui-icon-close fade fade-in-hover"></a>'));

			$('#notif-flash-wrapper').append($notif);
		}
	};
})(jQuery);


/**
 * Plug-in Jquery >> Disable on AJAX
 * V0.2
 * 
 * Désactive un bouton lors d'un appel AJAX
 * 
**/ 
(function($) {
	$.fn.disableOnAjax = function(options)
	{
		var defauts =
		{
			'img'	: false,
			'icon'	: false
		}; 
		var parametres = $.extend(defauts, options);

		return this.each(function()
		{
			var $this = $(this);
			if(parametres.img !== false) {
				$this.after('<img src="'+parametres.img+'" alt="" class="hide-begin" />');
				$(document).ajaxSend(function(){
					$this.attr('disabled', 'disabled').next().show();
					if($this.is('.ui-button'))$this.button('refresh');
				})
				.ajaxStop(function(){
					$this.removeAttr('disabled').next().hide();
					if($this.is('.ui-button'))$this.button('refresh');
				});
			}
			else if(parametres.icon !== false) {
				if($this.val() != '') {
					$this.after('<span class="icon-progress" />');
					$(document).ajaxSend(function(){
						$this.attr('disabled', 'disabled').next().show();
						if($this.is('.ui-button'))$this.button('refresh');
					})
					.ajaxStop(function(){
						$this.removeAttr('disabled').next().hide();
						if($this.is('.ui-button'))$this.button('refresh');
					});
				}
				else if($this.find('.ui-button-text').length) {
					var $txt_btn = $this.find('.ui-button-text').html();
					$(document).ajaxSend(function(){
						$this.attr('disabled', 'disabled').find('.ui-button-text').html($txt_btn+' <span class="icon-progress"></span>');
						if($this.is('.ui-button'))$this.button('refresh');
					})
					.ajaxStop(function(){
						$this.removeAttr('disabled').find('.ui-button-text').html($txt_btn);
						if($this.is('.ui-button'))$this.button('refresh');
					});
				}
				else {
					var $txt = $this.html();
					$(document).ajaxSend(function(){
						$this.attr('disabled', 'disabled').html($txt+' <span class="icon-progress"></span>');
						if($this.is('.ui-button'))$this.button('refresh');
					})
					.ajaxStop(function(){
						$this.removeAttr('disabled').html($txt);
						if($this.is('.ui-button'))$this.button('refresh');
					});
				}
			}
			else {
				$(document).ajaxSend(function(){
					$this.attr('disabled', 'disabled');
					if($this.is('.ui-button'))	$this.button('refresh');
				})
				.ajaxStop(function(){
					$this.removeAttr('disabled');
					if($this.is('.ui-button'))	$this.button('refresh');
				});
			}
			
		});
	}
})(jQuery);

/**
 * Ajout de DisableSelection et EnableSelection : Deprecié a partir de JQuery UI 1.10
 * Ajout sur core.js si n'existe pas déjà
 * Permet de stopper la séléction par défaut d'un élément
 **/
if(!jQuery().disableSelection) {
	$.fn.extend({
		disableSelection: function() {
			return this.bind( ( $.support.selectstart ? "selectstart" : "mousedown" ) +
				".ui-disableSelection", function( event ) {
					event.preventDefault();
				});
		},

		enableSelection: function() {
			return this.unbind( ".ui-disableSelection" );
		}
	});
}


/* -- */

$(function(){
	
	/* Création des boutons */
	$('.btn, button, input[type=submit], input[type=reset], input[type=button]').button();
	
	/* Loader général */
	$(document).ajaxStart(function(){
		$("#ajax-loader").stop(true).fadeIn('slow');
	})
	.ajaxStop(function(){
		$("#ajax-loader").stop(true).fadeOut('fast');
	});
	
	/* liens */
	if($('a.external').length) $('a.external').after('<span class="ui-icon ui-icon-extlink" />');
	if($('a.new-tab').length)  $('a.new-tab').after('<span class="ui-icon ui-icon-newwin" />');
});