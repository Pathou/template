$(function() {
	
	/*$(window).on('resize', function() {
		$modif = $('#modif');
		$modif.find('.input-append, .input-prepend').each(function() {
			$(this).children('input').outerWidth($(this).outerWidth()-$(this).children('span').outerWidth());
		});
	});*/
	
	$('#modif form').submit(function(e) {
		e.preventDefault();
		$.post('scss.ajax.php', $(this).serialize(), function(data) {
			if(data.ok == 'ok') {
				$('#css').attr('href', data.file);
			}
			else {
				$('#scss-error').html('Erreur '+data[0]);
			}
		}, 'json');
	});
	
	$('.buttonset').buttonset();
	
	$('form').filter(':not(#form-confirm)').submit( function(event) {
		event.preventDefault();
	});
	
	/* loader sur boutons */
	$('.btn-loader').disableOnAjax();
	$('.btn-loader-img').disableOnAjax({'img': 'img/ajax-loader.gif'});
	$('.btn-loader-icon').disableOnAjax({'icon': true});
	
	$('#submit_ajax_loader, .btn-loader, .btn-loader-img, .btn-loader-icon').click( function() {
		$.post('test.php');
	});
	
	/* Dropdown */
	$('.dropdown').dropdown();
	
	/* Confirm */
	$('.confirm').confirm();
	$('.confirm-params-js').confirm({
		title: 'Mon titre',
		message: 'Mon Message<br />Sur deux lignes',
		button_ok: 'OUI !',
		button_cancel: 'NON !',
		button_cancel_icon_primary: 'ui-icon-alert',
		ok: function() {
			$('#confirm-param-js-output').attr('class', 'well success').html('Ok !');
		},
		cancel: function () {
			$('#confirm-param-js-output').attr('class', 'well error').html('Canceled !');
		}
	});
	
	//$('.tabs').tabs();
	$('#carousel-1-tabs').tabs();
	$('#carousel-2-tabs').tabs();
	$('#carousel-3-tabs').tabs({ disabled: [2] });
	$('#carousel-4-tabs').tabs();
	
	$('#pagination-tabs').tabs({ disabled: [2] });
	$('#pagination-2-tabs').tabs({ disabled: [2] });
	$('#pagination-3-tabs').tabs();
	$('#pagination-4-tabs').tabs();
	
	$('#pagination-ajax-tabs').tabs({ disabled: [2] });
	$('#pagination-ajax-2-tabs').tabs({ disabled: [2] });
	
	$('#infinite-scroll-tabs').tabs();

	$('.toggle').toggleIt();
	
	$('.bg-btn').click( function() {
		$('body').toggleClass('bg', 'fade');
	});
	
	/* Carousel */
	
	$('#carousel').carousel()
		.bind('carousel', function(event, direction) { 
			$('#carousel-result').html('slide numéro: '+$(this).attr('data-current-num') +' - '+ direction);
	});
	$('#carousel-2').carousel({
		vertical: true,
		loop: true
	});
	$('#carousel-3').carousel({
		can_disable: false,
		duration: 'slow', /* voir http://jqueryui.com/demos/effect/easing.html */
		easing: 'easeInOutQuart',
		onbeforeslide: function () {
			$('#carousel-3-result').append('<span class="blue">Before Slide ! n° '+$('#carousel-3').attr('data-current-num')+'</span><br />').children().delay(3000).fadeOut('slow', function() {$(this).remove();});
		},
		onslide: function () {
			$('#carousel-3-result').append('<span class="red">Sliding ! n° '+$('#carousel-3').attr('data-current-num')+'</span><br />').children().delay(3000).fadeOut('slow', function() {$(this).remove();});
		},
		onafterslide: function () {
			$('#carousel-3-result').append('<span class="green">After Slide ! n° '+$('#carousel-3').attr('data-current-num')+'</span><br />').children().delay(3000).fadeOut('slow', function() {$(this).remove();});
		}
	});
	$('#carousel-4').carousel({
		nb_shown: 2,
		nb_move: 2
	});
	$('#carousel-4-pages div').click( function() { $('#carousel-4').trigger('slideTo', $(this).html()); });
	$('#carousel-5').carousel({
		autoSlide: true,
		autoSlide_duration: 2000
	});
	
	/* Pagination */
	
	$('#pagination').pagination({
		target: 'pagination-contenu',
		nb_by_pages: 3
	});
	
	$('#pagination-2').pagination({
		target: 'pagination-2-contenu',
		classes: 'clickable black',
		effect: 'drop',
		nb_by_pages: 5,
		nb_shown: 3
	});
	
	$('#pagination-3').pagination({
		target: 'pagination-3-contenu',
		classes: 'well white',
		nb_by_pages: 2,
		nb_shown: 3
	});
	
	$('#pagination-4').pagination({
		target: 'pagination-4-contenu',
		classes: 'my-pagination-custom',
		nb_by_pages: 2,
		nb_shown: 3
	});
	
	$('#pagination-ajax').pagination({
		target: 'pagination-ajax-contenu',
		classes: 'clickable white',
		effect: 'explode',
		speed_out: 'slow',
		speed_in: 'slow',
		nb_by_pages: 2,
		nb_shown: 3,
		ajax: {
			'url' : 'pagination.ajax.php',
			'nb_total': 25,
			'datas'	: {'ma_data': 15}
		}
	});
	
	$('#pagination-ajax-2').pagination({
		target: 'pagination-ajax-2-contenu',
		classes: 'clickable',
		method: 'post',
		nb_by_pages: 5, nb_shown: 2,
		ajax: {
			'url' : 'pagination.ajax.php',
			'nb_total': 25,
			'json': true
		}
	});
	
	$('#pagination-toggle').hide();
	
	/* Infinite Scroll */

	$('#infinite-scroll').infiniteScroll({
		nb_to_show: 6,
		'speed_out': 'slow',
		'speed_in' : 'slow',
		ajax: {
			'url' : 'infiniteScroll.ajax.php',
			'json': true,
			'datas'	: {'ma_data': 15}
		}
	});
	
	$('.t-countdown').textarea_countdown();
	
	
	$.flash.top = '45px';
	$('#btn-flash-1').click(function() {
		$.flash.add('Ma simple notification');
	});
	
	$('#btn-flash-2').click(function() {
		$.flash.add({
			text: "Notif <strong>modifiée</strong>",
			classes: 'well error shadow-outset',
			delay: 4000,
			effect: 'drop',
			effect_options: {'direction': 'right'},
			speed_in: 'fast',
			speed_out: 'slow',
			cross_close: true
		});
	});

});