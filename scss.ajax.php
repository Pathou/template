<?php
session_start();
require "scss.inc.php";
$scss = new scssc();
$scss->setImportPaths("css/");

$scss_file = file_get_contents('css/core-scss.scss');

// scss_formatter -> v.dev; scss_formatter_compressed -> v.production
if(isset($_POST['type_sortie']) && $_POST['type_sortie'] == 'compress') $formatter = 'scss_formatter_compressed';
else $formatter = 'scss_formatter';
$scss->setFormatter($formatter);

// will search for `test/style.scss'
//echo $scss->compile('@import "core-scss-'.$uniqid.'.scss"')
$uniqid = session_id();
if(file_put_contents('css/scss_output/core-'.$uniqid.'.css', $scss->compile(scss_replace($scss_file))) ){
	echo json_encode(array('ok' => 'ok', 'file' => 'css/scss_output/core-'.$uniqid.'.css?'.uniqid()));
}



function scss_replace($scss) {
	
	$pattern = array(
		'#\$color-grey: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-black: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-white: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-red: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-orange: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-yellow: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-green: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-blue: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-violet: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-pink: ([a-zA-Z0-9\#]+)\;#iU',

		'#\$color-success: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-error: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$color-highlight: ([a-zA-Z0-9\#]+)\;#iU',
		
		'#\$well-border-radius: ([a-zA-Z0-9 ]+)\;#iU',

		'#\$small-size: ([a-zA-Z0-9\.]+)\;#iU',
		'#\$big-size: ([a-zA-Z0-9\.]+)\;#iU',

		'#\$table-strip-color: ([a-zA-Z0-9\#]+)\;#iU',
		'#\$table-border-color: ([a-zA-Z0-9\#]+)\;#iU',

		'#\$fade-opacity: ([0-9\.]+)\;#iU',
		'#\$fade-transition-speed: ([a-z0-9\.]+)\;#iU',

		'#\$label-radius: ([a-zA-Z0-9 ]+)\;#iU',
		'#\$label-padding: ([a-zA-Z0-9 ]+)\;#iU',

		'#\$badge-radius: ([a-zA-Z0-9 ]+)\;#iU',
		'#\$badge-padding: ([a-zA-Z0-9 ]+)\;#iU',

		'#\$label-badge-shadow-size: ([a-zA-Z0-9 \.]+)\;#iU',
		'#\$label-badge-shadow-speed: ([a-z0-9\.]+)\;#iU',

		'#\$input-text-border-radius: ([a-zA-Z0-9 ]+)\;#iU',
	);
	
	$replace = array(
		'$color-grey: '.$_POST['color_grey'].';',
		'$color-black: '.$_POST['color_black'].';',
		'$color-white: '.$_POST['color_white'].';',
		'$color-red: '.$_POST['color_red'].';',
		'$color-orange: '.$_POST['color_orange'].';',
		'$color-yellow: '.$_POST['color_yellow'].';',
		'$color-green: '.$_POST['color_green'].';',
		'$color-blue: '.$_POST['color_blue'].';',
		'$color-violet: '.$_POST['color_violet'].';',
		'$color-pink: '.$_POST['color_pink'].';',

		'$color-success: '.$_POST['color_success'].';',
		'$color-error: '.$_POST['color_error'].';',
		'$color-highlight: '.$_POST['color_highlight'].';',
		
		'$well-border-radius: '.$_POST['well_border_radius'].';',

		'$small-size: '.$_POST['small_size'].';',
		'$big-size: '.$_POST['big_size'].';',

		'$table-strip-color: '.$_POST['table_strip_color'].';',
		'$table-border-color: '.$_POST['table_border_color'].';',

		'$fade-opacity: '.$_POST['fade_opacity'].';',
		'$fade-transition-speed: '.$_POST['fade_transition_speed'].';',

		'$label-radius: '.$_POST['label_radius'].';',
		'$label-padding: '.$_POST['label_padding'].';',

		'$badge-radius: '.$_POST['badge_radius'].';',
		'$badge-padding: '.$_POST['badge_padding'].';',

		'$label-badge-shadow-size: '.$_POST['label_badge_shadow_size'].';',
		'$label-badge-shadow-speed: '.$_POST['label_badge_shadow_speed'].';',
		
		'$input-text-border-radius: '.$_POST['input_text_border_radius'].';',
	);
	
	$scss = preg_replace($pattern, $replace, $scss, 1);
	
	return $scss;
}
?>